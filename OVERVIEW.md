
Yewberry Overview
=================

Basic syntax
------------

Yewberry code is like Scheme and Lisp, using S-expressions
between `(` and `)` parentheses.  There are also math-like
expressions with infix and unary operators.

Comments are introduced by the `;` character and extend to the
end of the line.  There are no block comments, and no system
for quoting code.

Identifiers can contain not only alpha-numeric characters, but
also various symbols and punctuation marks.  Hence something
like `foo&7` which is considered to be three tokens in languages
like C, C++ and Go, is just a single identifier in Yewberry.


Types
-----

There are five fundamental types:

- Int : 32-bit signed integers
- String : UTF-8 encoded strings
- Array : growable 0-indexed arrays
- Map : associative dictionaries, keys are strings
- Func : anonymous function with N parameters


NIL and TRUE
------------

There is no boolean type, but two integer values are treated
specially: the highest integer is "TRUE", and the lowest integer
is "NIL".  When a boolean value is required, every value except
NIL is considered to be true.  NIL is the only value to be
considered to be false.

NIL is also produced when accessing an array with an index
which is out-of-bounds, or accessing a map when the key does
not exist in the map.  Certain forms, like assignment and for
loops, have no natural result and hence always produce NIL.


Primary forms
-------------

In the following list, elements between `[` and `]` are optional,
and elements between `{` and `}` can be repeated zero or more
times.  `BLOCK` is a sequence of one or more expressions or var
forms.

```
(var name value)
(fun name "(" { param } ")" BLOCK)
(lam "(" { param } ")" BLOCK)
(if cond BLOCK { "elif" cond BLOCK } [ "else" BLOCK ] )
(begin { expr })
(set! var { indexor } value)
```


Math expressions
----------------

An expression within `(` and `)` may be a math expression,
which is detected when it contains at least one operator.
Math expressions consist of terms, like numbers or normal
function calls, and operators, either unary or binary (infix)
operators.  The precedence of binary operators determines
the order of evaluation, as you would expect.

For example:
```
(1 + 2 * 3)  ; result is 7
```

Each operator in Yewberry has a corresponding function which
uses a word instead of a mathematical symbol.  For example,
the `*` operator is mapped to the `mul` function.  The coding
convention is to use the operator syntax, except when that is
not possible, e.g. passing `lt?` as a predicate to `sort!`.

See the Numbers section below for a full list of operators.
Some operators support both integers and strings.


Loop forms
----------

In the following, `I`, `K` and `V` represent user-supplied
variable names which take each index, key or value.
None of these forms produce any result (it is always NIL).
There is no mechanism to break out of a loop.

```
(while cond BLOCK)
(for I start end BLOCK)
(for-each I V array BLOCK)
(for-each K V map BLOCK)
```


Built-in constants
------------------

```
(var TRUE 0x7fffffff)
(var NIL  0x80000000)
(var EOF  "\x1A")
```


Arrays
------

Arrays can be constructed using the syntax:
```
[ elem1 elem2 ... ]
```

Arrays can be read and written using syntax which is similar
to function calls.  Multiple dimensions are supported by using
two or more index values.
```
(var A [1 2 3])
(print ($ (A 2)))  ; displays "3"
(set! A 1 4)       ; modifies A to [1 4 3]
```

The following builtins for arrays are available:
```
(len A)
(empty? A)
(copy A)
(subseq A start end)
(same-obj? A1 A2)

(append! A value)
(insert! A index value)
(delete! A index)
(join! A B)

(map! A predicate)
(filter! A predicate)
(sort! A predicate)
(find A predicate)
```


Maps
----

The keys of maps are always strings, however a special syntax
makes accessing maps look a lot nicer (like accessing structs
in languages like C and Go).  An identifier that begins with
a period is equivalent to a string.  For example: `.name` is
equivalent to `"name"`.

Maps can be constructed with the following syntax:
```
{ key1 value1 key2 value2 ... }
```

Maps are read and written using the same syntax as arrays
(and similar to function calls), for example:
```
(var M {.name "Fred" .age 35})
(print (M .name))   ; displays: Fred
(has? M .sister)    ; checks if "sister" field exists
(set! M .age 37)    ; modifies the "age" field
```

The following builtins for maps are available:
```
(len M)      ; total number of key/value pairs
(empty? M)
(has? M key)
(same-obj? M1 M2)

(copy M)
(delete! M key)
(join! M M2)
```


Characters
----------

Characters are integer values which represent Unicode code
points.  They can be used to construct or examine strings.

The following six builtins are for testing the general class of
a character.  The term "mark" refers to combining characters,
ones used to modify a previous character to add a small shape.
Note that there are some characters which do not appear in any
of these classes.
```
(char-letter? C)
(char-digit? C)
(char-symbol? C)
(char-mark? C)
(char-space? C)
(char-control? C)
```

A few other builtins are available for characters:
```
(char-lower C)
(char-upper C)
```


Strings
-------

Strings are read-only sequences of Unicode characters.
Strings can be accessed as if they were arrays, producing an
integer value for the character (or NIL when the index is
out-of-bounds).

Strings can be concatenated using the `+` operator.  The
special `$` form can be used to convert any value into a
string, and will concatenate all of its arguments using a
single space as the separator.  For example:
```
($ "Sum is:" (1 + 2))   ; result is "Sum is: 3"
```

Strings can be compared using the comparison operators
(see the Numbers section below).

The following builtins are available for strings:
```
(len S)
(empty? S)
(subseq S start end)

(min S1 S2)
(max S1 S2)
(lowercase S)
(uppercase S)

(hex$ integer)   ; converts to hexadecimal form
(char$ integer)  ; converts a character to a string
(quote$ S)       ; makes a quoted string (with escapes)
(parse S)        ; can decode most yewberry literals
```


Numbers
-------

Only 32-bit integers are currently supported by Yewberry.
In the future, 64-bit floating point may be supported.
Note that a full numeric tower like Scheme is extremely
unlikely.

The basic math operations are:
```
(L + R)   ; (add L R)
(L - R)   ; (sub L R)
(L * R)   ; (mul L R)
(L / R)   ; (div L R)
(L % R)   ; (mod L R)
(L ** R)  ; (pow L R)

(abs N)
(min N1 N2)
(max N1 N2)
```

Bit-wise operations:
```
(L & R)   ; (bit-and L R)
(L | R)   ; (bit-or  L R)
(L ^ R)   ; (bit-xor L R)

(bit-not N)

(N << shift)  ; (shift-left  N shift)
(N >> shift)  ; (shift-right N shift)
```

Comparison operations:
```
(L == R)   ; (eq? L R)
(L != R)   ; (ne? L R)
(L <  R)   ; (lt? L R)
(L <= R)   ; (le? L R)
(L >  R)   ; (gt? L R)
(L >= R)   ; (ge? L R)
```

Boolean logic:
```
(L && R)   ; (and L R)
(L || R)   ; (or  L R)

(bool-not B)
```


Other builtins
--------------

Checking the type of a value:
```
(type-int? xx)
(type-string? xx)
(type-array? xx)
(type-map? xx)
(type-fun? xx)
```

Random numbers:
```
(rand-seed seed)
(rand-int modulo)
(rand-range low high)
```

Input and output:
```
(enable-editor)
(input prompt)
(print str)
```

Miscellaneous:
```
(error str)
```


Example program
---------------

```
;;
;; Compute the Fibonacci sequence.
;;

; this is a naive, recursive, slow implementation
(fun fib (n)
  (if (n < 2)
    n
  else
    (var A (fib (n - 2)))
    (var B (fib (n - 1)))
    (A + B)
  )
)

; display the sequence, see how far it gets on your machine!
(for i 0 99
  (print ($ "fib" i "=" (fib i)))
)
```
