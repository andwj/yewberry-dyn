// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

// This is some experimental code to automatically format
// yewberry code.  The results are quite poor at the moment,
// plus user comments are not handled yet.

package main

import "os"
import "io"
import "fmt"
import "strings"

type Snap struct {
	tab int
}

func FormatFile(r io.Reader) {
	lex := NewLexer(r)

	seen := false

	for {
		t := lex.Parse()

		if t.Kind == TOK_EOF {
			break
		}
		if t.Kind == TOK_ERROR {
			fmt.Fprintf(os.Stderr, "\nERROR on line %d: %s\n", t.LineNum, t.Str)
			os.Exit(1)
		}

		// add a blank line between top-level definitions
		if seen {
			fmt.Printf("\n")
		}
		seen = true

		var snap Snap

		FormatToken(t, &snap)
	}
}

func FormatToken(t *Token, snap *Snap) {
	if t.Kind == TOK_String {
		fmt.Printf("%*s%q\n", snap.tab, "", t.Str)
		return
	}

	if t.Kind < TOK_Expr {
		fmt.Printf("%*s%s\n", snap.tab, "", t.Str)
		return
	}

	if Depth(t) <= 2 {
		s := Unbroken(t)
		fmt.Printf("%*s%s\n", snap.tab, "", s)
		return
	}

	start, end := Brackets(t)

	children := t.Children

	if t.Kind == TOK_Expr &&
		len(children) > 0 &&
		children[0].Kind < TOK_Expr {

		fmt.Printf("%*s%c%s\n", snap.tab, "", start, children[0].Str)

		children = children[1:]
	} else {
		fmt.Printf("%*s%c\n", snap.tab, "", start)
	}

	snap.tab += 2

	for _, sub := range children {
		FormatToken(sub, snap)
	}

	snap.tab -= 2
	fmt.Printf("%*s%c\n", snap.tab, "", end)
}

func Unbroken(t *Token) string {
	switch t.Kind {
	case TOK_Name, TOK_Int, TOK_Float:
		return t.Str

	case TOK_String:
		return fmt.Sprintf("%q", t.Str)

	case TOK_Char:
		r := []rune(t.Str)

		if len(r) == 0 {
			return "''"
		} else {
			return fmt.Sprintf("%q", r[0])
		}
	}

	if t.Children == nil {
		return "!!!WEIRD!!!"
	}

	var sb strings.Builder

	start, end := Brackets(t)

	sb.WriteRune(start)

	for i, sub := range t.Children {
		if i > 0 {
			sb.WriteString(" ")
		}

		sb.WriteString(Unbroken(sub))
	}

	sb.WriteRune(end)

	return sb.String()
}

func Depth(t *Token) int {
	depth := 0

	if t.Kind >= TOK_Expr {
		for _, sub := range t.Children {
			d := Depth(sub) + 1
			if d > depth {
				depth = d
			}
		}
	}

	return depth
}

func Brackets(t *Token) (s, e rune) {
	s = '('
	e = ')'

	switch t.Kind {
	case TOK_Array:
		s, e = '[', ']'
	case TOK_Map:
		s, e = '{', '}'
	}

	return
}
