// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
import "io"
import "fmt"
import "strings"

import "time"
import "math/rand"
import "path/filepath"

import "github.com/peterh/liner"

var Ok error = nil

type GlobalDef struct {
	// memory loc for the value
	loc *Value

	// its name (for debugging)
	name string
}

// all the global definitions
var globals []*GlobalDef
var global_lookup map[string]int

const GLOB_NIL = 0
const GLOB_TRUE = 1
const GLOB_EOF = 2

// the unparsed expressions of a program, which get compiled and
// executed after all the global definitions are handled.
var expressions []*Token

// this is true when the REPL is active (interactive session).
var REPL bool

// the command-line editor for terminal input.
// always available in REPL, must be explicitly enabled in scripts.
var editor *liner.State

var ErrQuit = fmt.Errorf("user quit")
var ErrAbort = fmt.Errorf("user abort")
var ErrInput = fmt.Errorf("input error")

//----------------------------------------------------------------------

func main() {
	SetupGlobals()
	SetupBuiltins()
	SetupOperators()
	SetupRandom()
	SetupENVS()

	status := 0

	if len(os.Args) < 2 {
		SetupARGS([]string{"REPL"})

		status = RunREPL()

	} else {
		// skip the interpreter itself, have source file at (ARGS 0)
		SetupARGS(os.Args[1:])

		status = RunScriptFile(os.Args[1])
	}

	os.Exit(status)
}

func SetupGlobals() {
	globals = make([]*GlobalDef, 0)
	global_lookup = make(map[string]int)

	MakeGlobal("NIL").loc.MakeInt(NIL)
	MakeGlobal("TRUE").loc.MakeInt(TRUE)
	MakeGlobal("EOF").loc.MakeString("\x1A")

	MakeGlobal("MAX-LOOP").loc.MakeInt(22111000)
	MakeGlobal("STACK-TRACE").loc.MakeInt(8)
	MakeGlobal("DUMP-CODE").loc.MakeInt(NIL)
}

func SetupRandom() {
	rand.Seed(time.Now().Unix())
}

func SetupARGS(args []string) {
	ARGS := MakeGlobal("ARGS").loc
	ARGS.MakeArray(0)

	for _, s := range args {
		str_val := Value{}
		str_val.MakeString(s)

		ARGS.AppendElem(str_val)
	}
}

func SetupENVS() {
	ENVS := MakeGlobal("ENVS").loc
	ENVS.MakeMap()

	var val_str Value

	for _, env := range os.Environ() {
		key, val := SplitEnvString(env)

		if key != "" {
			val_str.MakeString(val)
			ENVS.KeySet(key, val_str)
		}
	}
}

func SplitEnvString(env string) (key, val string) {
	// find the '=' between key and value
	for i := 0; i < len(env); i++ {
		if env[i] == '=' {
			return env[0:i], env[i+1:]
		}
	}

	// something weird, ignore it
	return "", ""
}

func RunREPL() (status int) {
	REPL = true

	EnableEditor()

	// this will reset the terminal into a usable state should
	// a panic occur somewhere.
	// [ unfortunately there is no way to recover from serious
	//   runtime issues like overflowing the Go stack... ]
	defer editor.Close()

	for {
		err := ProcessUserInput()

		switch {
		case err == ErrQuit:
			return 0

		case err == ErrInput:
			return 1

		case err == ErrAbort:
			return 2
		}
	}
}

func EnableEditor() {
	if editor == nil {
		editor = liner.NewLiner()
		editor.SetCtrlCAborts(true)
	}
}

func ProcessUserInput() error {
	code := ""

	for {
		prompt := "> "
		if code != "" {
			prompt = ">> "
		}

		s, err := editor.Prompt(prompt)

		if err == io.EOF {
			// CTRL-D on a continuation line is a cancel
			if prompt == ">> " {
				fmt.Fprintf(os.Stderr, "\nCancelled\n")
				return Ok
			}

			fmt.Fprintf(os.Stderr, "\nEOF\n")
			return ErrQuit
		}

		if err == liner.ErrPromptAborted {
			fmt.Fprintf(os.Stderr, "Aborted\n")
			return ErrAbort
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, "Input Error: %s\n", err.Error())
			return ErrInput
		}

		// ignore blank lines
		if s == "" {
			continue
		}

		if code == "" {
			code = s
		} else {
			code = code + " " + s
		}

		if !CheckUnterminated(code) {
			break
		}
	}

	editor.AppendHistory(code)

	// see if user wants to quit
	switch code {
	case "q", "Q", "quit", "QUIT", "exit", "EXIT":
		return ErrQuit
	}

	lex := NewLexer(strings.NewReader(code))

	err := ProcessCode(lex)

	if err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
	}

	// the REPL can ignore compile/runtime errors
	return Ok
}

func CheckUnterminated(code string) bool {
	// use the lexer to see if there is an unclosed bracket

	lex := NewLexer(strings.NewReader(code))

	for {
		t := lex.Parse()

		if t.Kind == TOK_EOF {
			return false
		}

		if t.Kind == TOK_ERROR {
			if strings.Contains(t.Str, "unterminated expr") {
				return true
			}
			return false
		}
	}
}

//----------------------------------------------------------------------

func RunScriptFile(filename string) (status int) {
	// if the script uses the command line editor, this will reset
	// the terminal into a usable state should a panic occur.
	defer func() {
		if editor != nil {
			editor.Close()
		}
	}()

	f, err := os.Open(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		return 1
	}
	defer f.Close()

	lex := NewLexer(f)
	/// lex.DumpTokens()

	err = ProcessCode(lex)

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())

		err = LocErrorWithFile(err, filename)

		switch t := err.(type) {
		case *LocError:
			s := t.Location()
			if s != "" {
				fmt.Fprintf(os.Stderr, "Occurred%s\n", s)
			}
		}

		return 1
	}

	// status zero means Ok
	return 0
}

func ProcessCode(lex *Lexer) error {
	var err error

	expressions = make([]*Token, 0)

	for {
		t := lex.Parse()

		if t.Kind == TOK_ERROR {
			return LocErrorf(t.LineNum, "%s", t.Str)
		}

		if t.Kind == TOK_EOF {
			break
		}

		err = ParseToken(t)
		if err != nil {
			// fallback line number in case we don't have any
			return LocErrorAt(err, t.LineNum)
		}
	}

	err = CompileAllFunctions()
	if err != nil {
		return err
	}

	err = RunAllExpressions()
	if err != nil {
		return err
	}

	return Ok
}

func ParseToken(t *Token) error {
	if t.Kind == TOK_Expr {
		if len(t.Children) == 0 {
			return LocErrorf(t.LineNum, "empty expr in ()")
		}

		head := t.Children[0]

		if head.Match("var") {
			return ParseGlobalVar(t.Children)
		}

		if head.Match("fun") {
			return ParseFuncDef(t.Children)
		}
	}

	return ParseExpression(t)
}

func ParseGlobalVar(children []*Token) error {
	lno := children[0].LineNum

	if len(children) < 2 {
		return LocErrorf(lno, "bad variable def: missing name")
	}
	if len(children) < 3 {
		return LocErrorf(lno, "bad variable def: missing value")
	}
	if len(children) > 3 {
		return LocErrorf(lno, "bad variable def: too many values")
	}

	name := children[1]
	val := children[2]

	if name.Kind != TOK_Name {
		return LocErrorf(lno, "bad variable def: name is not an identifier")
	}

	// check for reserved keywords like "var", "if", "else"
	if !ValidDefName(name.Str) {
		return LocErrorf(lno, "bad variable def: '%s' is a reserved word", name.Str)
	}

	// check for defining a variable to itself
	if val.Kind == TOK_Name && val.Str == name.Str {
		return LocErrorf(lno, "cannot define '%s' as itself", name.Str)
	}

	// global defs normally cannot be redefined, but the REPL allows it
	_, exist := global_lookup[name.Str]
	if exist && !REPL {
		return LocErrorf(lno, "global '%s' is already defined", name.Str)
	}

	def := MakeGlobal(name.Str)

	err := def.loc.ParseLiteral(val)
	if err != nil {
		return LocErrorAt(err, lno)
	}

	return Ok
}

func ParseFuncDef(children []*Token) error {
	lno := children[0].LineNum

	if len(children) < 2 {
		return LocErrorf(lno, "bad function def: missing name")
	}
	if len(children) < 4 {
		return LocErrorf(lno, "bad function def: missing params or body")
	}

	name := children[1]

	if name.Kind != TOK_Name {
		return LocErrorf(lno, "bad function def: name is not an identifier")
	}

	// check for reserved keywords like "fun", "if", "else"
	if !ValidDefName(name.Str) {
		return LocErrorf(lno, "bad function def: '%s' is a reserved word", name.Str)
	}

	// global defs normally cannot be redefined, but the REPL allows it
	_, exist := global_lookup[name.Str]
	if exist && !REPL {
		return LocErrorf(lno, "global '%s' is already defined", name.Str)
	}

	def := MakeGlobal(name.Str)

	cl := new(Closure)
	cl.uncompiled = children[2:]
	cl.debug_name = name.Str

	def.loc.MakeFunction(cl)

	return Ok
}

func ParseExpression(t *Token) error {
	// we merely remember the raw tokens here, we process it later
	expressions = append(expressions, t)

	return Ok
}

func ValidDefName(name string) bool {
	switch name {
	case "var", "fun", "lam", "method", "type", "macro":
		return false

	case "begin", "set!", "self":
		return false

	case "if", "else", "for", "for-each", "while":
		return false

	case "NIL", "TRUE", "EOF", "ARGS":
		return false

	case ":", "=", "$", "->", "...":
		return false
	}

	// disallow names of the operators
	_, exist := operators[name]
	if exist {
		return false
	}

	// disallow names of the builtins
	g_idx, exist := global_lookup[name]
	if exist {
		loc := globals[g_idx].loc
		if loc.Clos != nil && loc.Clos.builtin != nil {
			return false
		}
	}

	return true
}

//----------------------------------------------------------------------

func CompileAllFunctions() error {
	for _, def := range globals {
		name := def.name
		loc := def.loc

		if loc.Kind == VAL_Function &&
			loc.Clos.builtin == nil &&
			loc.Clos.vm_ops == nil {

			err := CompileFunction(loc.Clos, name)
			if err != nil {
				// remove the global (don't leave a dud one)
				DeleteGlobal(name)

				return LocErrorWithFunc(err, name)
			}
		}
	}

	return Ok
}

func RunAllExpressions() error {
	for _, tok := range expressions {
		err := RunExpression(tok)

		if err != nil {
			return err
		}
	}

	return Ok
}

func RunExpression(t *Token) error {
	var result Value

	did_print = false

	if t.Kind == TOK_Expr {
		cl, err := CompileRawExpr(t)
		if err != nil {
			return err
		}

		result, err = RunTop(cl)
		if err != nil {
			return err
		}

	} else if REPL {
		// parse it as a literal (in global scope)
		err := result.ParseLiteral(t)
		if err != nil {
			return err
		}

	} else {
		// we disallow bare literals in a code file
		return LocErrorf(t.LineNum, "expected expr in (), got: %s", t.String())
	}

	// display the result
	if REPL && !did_print {
		s := result.DeepString()

		fmt.Printf("%s\n", s)
	}

	return Ok
}

//----------------------------------------------------------------------

type LocError struct {
	Str  string
	Line int    // line # where error occurred, or 0
	File string // filename of error, or ""
	Func string // function which was being compiled, or ""
}

func LocErrorf(line int, format string, a ...interface{}) error {
	format = fmt.Sprintf(format, a...)

	return &LocError{format, line, "", ""}
}

func LocErrorAt(err error, line int) error {
	switch t := err.(type) {
	case *LocError:
		// an existing line number will be more accurate than the new one
		if t.Line == 0 {
			t.Line = line
		}
		return err
	}

	format := fmt.Sprintf("%s", err.Error())

	return &LocError{format, line, "", ""}
}

func LocErrorWithFile(err error, filename string) error {
	switch t := err.(type) {
	case *LocError:
		if t.File == "" {
			t.File = filename
		}
		return err
	}

	format := fmt.Sprintf("%s", err.Error())

	return &LocError{format, 0, filename, ""}
}

func LocErrorWithFunc(err error, funcname string) error {
	switch t := err.(type) {
	case *LocError:
		if t.Func == "" {
			t.Func = funcname
		}
		return err
	}

	format := fmt.Sprintf("%s", err.Error())

	return &LocError{format, 0, "", funcname}
}

func (err *LocError) Error() string {
	return err.Str
}

func (err *LocError) Location() string {
	l_part := ""
	if err.Line > 0 {
		l_part = fmt.Sprintf(" near line %d", err.Line)
	}

	f_part := ""
	if err.File != "" {
		f_part = " in " + filepath.Base(err.File)
	}

	g_part := ""
	if err.Func != "" {
		g_part = " in function '" + err.Func + "'"
	}

	return l_part + f_part + g_part
}
