
Yewberry README
===============

by Andrew Apted, 2018.


About
-----

Yewberry is a programming language which I wrote from scratch,
partly to see if I could do it (without looking at anybody else's
code or algorithms), partly to explore how minimal a language can
be and still be useful, and partly to serve as a base for future
experiments.

The language has a syntax like Scheme or LISP, with lexical scope,
and supports first-class functions, closures and tail calls.
Unlike Scheme or LISP, Yewberry does not have lists as a fundamental
construct, rather it has growable arrays and associative maps.
Numeric values are currently limited to 32-bit integers.

This repository holds an interpreter for the dynamically-typed
flavor of the language.  It implements a REPL, but can also run
code from a script file.

NOTE: there is also a statically-typed flavor of this language.
See the repository here: https://gitlab.com/andwj/yewberry


Status
------

The interpreter is complete and working, with no known issues.
There are two non-trivial sample programs: a small adventure
game and Conway's game of life, which are working fine in this
interpreter.


Documentation
-------------

The [Overview](OVERVIEW.md) contains a summary of the language.


Legalese
--------

Yewberry is Copyright &copy; 2018 Andrew Apted.

Yewberry is Free Software, under the terms of the GNU General
Public License, version 3 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

Yewberry comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.

