;;;
;;; A simple adventure game, in Yewberry.
;;;
;;; by Andrew Apted, 2018.
;;;
;;; this code is licensed as CC0 (i.e. public domain)
;;;

(var game-over NIL)

; main program
(begin
  (enable-editor)
  (welcome-message)
  (room-describe player-loc)
  (while (not game-over)
    (read-and-process-command))
  (quit-message)
)

(fun welcome-message ()
  (print "")
  (print "Welcome to a simple adventure game!")
  (print "")
)

(fun quit-message ()
  (print "Goodbye!")
)

(fun solved-message ()
  (print "You help yourself to the treasure.")
  (print "With your good health and new-found wealth,")
  (print "you live happily ever after....")
  (print "")
  (print "Congratulations, you solved the game!")
  (print "")
)

(fun read-and-process-command ()
  (var line (input "> "))
  ;; just quit on EOF [ nothing else we can do! ]
  ;; and ignore empty lines.
  (if (line == EOF)
    (set! game-over TRUE)
  else
    (parse-user-command line)
  )
)

(fun parse-user-command (line)
  (var words (split-words line))
  (if ((len words) > 0)
    (lowercase-words! words)
    (expand-abbr! words)
    ; remove dud words like "a" and "the"
    (filter! words valid-word?)
    (if ((len words) == 0)
      (print "Huh?")
    else
      (var verb (words 0))
      (var args (subseq words 1 999))
      (user-command verb args)
    )
  )
)

(fun user-command (verb args)
  ; replace any alias with the real verb
  (if ((ALIASES verb) != NIL)
    (set! verb (ALIASES verb))
  )
  (if ((COMMANDS verb) != NIL)
    ((COMMANDS verb) args)
  elif ((DIRS verb) != NIL)
    (cmd-go [verb])
  else
    (print ($ "I don't understand:" verb))
  )
)


;; Player Definitions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(var player-loc "Mountain")

(var player-found-key NIL)

(var player-inventory {"sword" TRUE})

(fun player-has-obj (obj)
  (player-inventory obj)
)

(fun player-add-obj (obj)
  (set! player-inventory obj TRUE)
)

(fun player-remove-obj (obj)
  (delete! player-inventory obj)
)


;; Room Definitions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(var DIRS {
  "n" "N"  "north" "N"
  "s" "S"  "south" "S"
  "e" "E"  "east" "E"
  "w" "W"  "west" "W"
  "d" "D"  "down" "D"
  "u" "U"  "up" "U"
})

(var ROOMS [
  {.name "Mountain"
   .exits {.N ["Forest" ""]}
   .objects {}
   .description [
     "You are standing on a large grassy mountain."
     "To the north you see a thick forest."
     "Other directions are blocked by steep cliffs." ]
  }

  {.name "Forest"
   .exits {.S ["Mountain" ""]
           .W ["Lake" ""]
           .E ["Outside" "CROCODILE"]}
   .objects {"crocodile" TRUE "parrot" TRUE }
   .description [
     "You are in a forest, surrounded by dense trees and shrubs."
     "A wide path slopes gently upwards to the south, and"
     "narrow paths lead east and west." ]
  }

  {.name "Lake"
   .exits {.E ["Forest" ""]}
   .objects {"steak" TRUE}
   .description [
     "You stand on the shore of a beautiful lake, soft sand under"
     "your feet.  The clear water looks warm and inviting." ]
  }

  {.name "Outside"
   .exits {.W ["Forest" ""]
           .E ["Castle" "KEY"]}
   .objects {}
   .description [
     "The forest is thinning off here.  To the east you can see a"
     "large castle made of dark brown stone.  A narrow path leads"
     "back into the forest to the west." ]
  }

  {.name "Castle"
   .exits {.W ["Outside" ""]
           .S ["Treasury" "PASSWORD"]}
   .objects {"guard" TRUE "carrot" TRUE}
   .description [
     "You are standing inside a magnificant, opulent castle."
     "A staircase leads to the upper levels, but unfortunately"
     "it is currently blocked off by delivery crates.  A large"
     "wooden door leads outside to the west, and a small door"
     "leads south." ]
  }

  {.name "Treasury"
   .exits {.N ["Castle" ""]}
   .objects {"treasure" TRUE}
   .description [
     "Wow!  This room is full of valuable treasures.  Gold, jewels,"
     "valuable antiques sit on sturdy shelves against the walls."
     "However...... perhaps money isn't everything??" ]
  }
])

(var PASSWORD "pinecone")

(fun room-find (name)
  (var idx (find ROOMS (lam (x) ((x .name) == name))))
  (if (idx == NIL)
    (error ($ "no such room: " name))
  else
    (ROOMS idx)
  )
)

(fun room-free-exit (room dir)
  (var ex ((room .exits) dir))
  (set! ex 1 "")
)

(fun room-has-obj (room obj)
  (var objects (room .objects))
  ((objects obj) != NIL)
)

(fun room-add-obj (room obj)
  (var objects (room .objects))
  (set! objects obj TRUE)
)

(fun room-remove-obj (room obj)
  (var objects (room .objects))
  (delete! objects obj)
)

(fun room-describe (name)
  (var room (room-find name))

  (for-each _ line (room .description)
    (print line)
  )
  (for-each obj _ (room .objects)
    (print ($ "There is a" obj "here."))
  )
)


;; Game Commands ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(fun cmd-quit (args)
  (set! game-over TRUE)
)

(fun cmd-help (args)
  (print "Use text commands to walk around and do things.")
  (print "")
  (print "Some examples:")
  (print "   go north")
  (print "   get the rope")
  (print "   drop the lantern")
  (print "   inventory")
  (print "   unlock door")
  (print "   kill the serpent")
  (print "   quit")
)

(fun cmd-inventory (args)
  (print "You are carrying:")

  (if (empty? player-inventory)
    (print "    nothing")
  else
    (for-each obj _ player-inventory
      (print ($ "    a" obj))
    )
  )
)

(fun cmd-look (args)
  (print "")
  (room-describe player-loc)
)

(fun cmd-go (args)
  (if (empty? args)
    (print "Go where?")
  else
    (var dir (DIRS (args 0)))
    (if (dir == NIL)
      (print "I don't understand that direction.")
    else
      (var room (room-find player-loc))
      (var ex ((room .exits) dir))
      (if (ex == NIL)
        (print "You cannot go that way.")
      else
        (var nextroom (ex 0))
        (var obstacle (ex 1))
        (if (obstacle == "KEY")
          (print "The castle door is locked!")
        elif (obstacle == "CROCODILE")
          (print "A huge, scary crocodile blocks your path!")
        elif (obstacle == "PASSWORD")
          (print "The guard stops you and says \"Hey, you cannot go in there")
          (print "unless you tell me the password!\".")
        else
          (set! player-loc nextroom)
          (print "")
          (room-describe player-loc)
        )
      )
    )
  )
)

(fun cmd-swim (args)
  (if (player-loc == "Outside")
    (print "But the moat is full of crocodiles!")
  elif (player-loc != "Lake")
    (print "There is nowhere to swim here.")
  elif player-found-key
    (print "You enjoy a nice swim in the lake.")
  else
    (print "You dive into the lake, enjoy paddling around for a while.")
    (print "Diving a bit deeper, you discover a rusty old key!")
    (set! player-found-key TRUE)
    (player-add-obj "key")
  )
)

(fun cmd-drop (args)
  (if (empty? args)
    (print "Drop what?")
  else
    (var obj (args 0))
    (var room (room-find player-loc))
    (if (not (player-has-obj obj))
      (print (($ "You are not carrying a" obj) + "."))
    else
      (player-remove-obj obj)
      (room-add-obj room obj)
      (print (($ "You drop the" obj) + "."))
    )
  )
)

(fun cmd-get (args)
  (if (empty? args)
    (print "Get what?")
  else
    (var obj (args 0))
    (var room (room-find player-loc))
    (if (not (room-has-obj room obj))
      (print ($ "You do not see any" obj "here."))
    elif (obj == "crocodile")
      (print "Are you serious?")
      (print "The only thing you would get is eaten!")
    elif (obj == "parrot")
      (print "The parrot nimbly evades your grasp.")
    elif (obj == "guard")
      (print "A momentary blush suggests the guard was flattered.")
    elif (obj == "treasure")
      (solved-message)
      (set! game-over TRUE)
    else
      (room-remove-obj room obj)
      (player-add-obj obj)
      (print (($ "You pick up the" obj) + "."))
    )
  )
)

(fun cmd-give (args)
  (if ((len args) < 2)
    (print "Give what to whom?")
  else
    (var obj (args 0))
    (var whom (args 1))
    (var room (room-find player-loc))

    (if (not (player-has-obj obj))
      (print ($ "You can't give a" (obj + ",")
                "since you don't have one!"))

    elif (not (room-has-obj room whom))
      (print ($ "There is no" whom "here."))

    elif (obj == "carrot" && whom == "parrot")
      (player-remove-obj obj)
      (print "The parrot happily starts munching on the carrot.  Every now")
      (print ($ "and then you hear it say" ("\"" + (PASSWORD + "\""))
                "as it nibbles away on that"))
      (print "orange stick.  I wonder who this parrot belonged to?")

    elif (obj == "steak" && whom == "crocodile")
      (player-remove-obj obj)
      (print "You hurl the steak towards the crocodile, which suddenly")
      (print "snaps into action, grabbing the steak in its steely jaws")
      (print "and slithering off to devour its meal in private.")
      (room-remove-obj room whom)
      (room-free-exit room "E")

    elif (whom == "parrot" || whom == "crocodile" || whom == "guard")
      (print ($ "The" whom "is not interested."))

    else
      (print "Don't be ridiculous!")
    )
  )
)

(fun cmd-feed (args)
  (if ((len args) < 2)
    (print "Feed what to whom?")
  else
    (cmd-give args)
  )
)

(fun cmd-open (args)
  (if (empty? args)
    (print "Open what?")
  else
    (var obj (args 0))
    (var room (room-find player-loc))
    (if (obj != "door")
      (print "You cannot open that.")
    elif (player-loc != "Outside")
      (print "There is no door here.")
    elif (not (player-has-obj "key"))
      (print "The door is locked!")
    else
      (print "Carefully you insert the rusty old key in the lock, and turn it.")
      (print "Yes!!  The door unlocks!  However the key breaks into several")
      (print "pieces and is useless now.")
      (player-remove-obj "key")
      (room-free-exit room "E")
    )
  )
)

(fun cmd-attack (args)
  (if (empty? args)
    (print "Attack what?")
  else
    (var target (args 0))
    (if (target == "crocodile")
      (print "The mere thought of wrestling with that savage beast")
      (print "paralyses you with fear!")

    elif (target == "guard" && (player-has-obj "sword"))
      (print "You and the guard begin a dangerous sword fight!")
      (print "But after ten minutes or so, you are both exhausted and")
      (print "decide to call it a draw.")

    elif (target == "guard")
      (print "You raise your hands to fight, then notice that the guard")
      (print "is carrying a sword, so you shadow box for a while instead.")

    elif (player-has-obj "sword")
      (print "You swing your sword, but miss!")

    else
      (print "You bruise your hand in the attempt.")
    )
  )
)

(fun cmd-use (args)
  (if (empty? args)
    (print "Use what?")
  else
    (var obj (args 0))
    (if (not (player-has-obj obj))
      (print "You cannot use something you don't have.")
    elif (obj == "key")
      (cmd-open ["door"])
    elif (obj == "sword")
      (print "You practise your parry skills.")
    else
      (print "Its lack of utility leads to futility.")
    )
  )
)

(fun cmd-say (args)
  (if (empty? args)
    (print "Say what?")
  else
    (var word (args 0))
    (var room (room-find player-loc))
    (if (word != PASSWORD || player-loc != "Castle")
      (print "Nothing happens.")
    else
      (print "The guard says \"Welcome Sire!\" and beckons you to enter")
      (print "the treasury.")
      (room-free-exit room "S")
    )
  )
)

(var COMMANDS {
  "quit" cmd-quit
  "inventory" cmd-inventory
  "help" cmd-help
  "look" cmd-look
  "go"   cmd-go
  "swim" cmd-swim
  "drop" cmd-drop
  "get"  cmd-get
  "give" cmd-give
  "feed" cmd-feed
  "open" cmd-open
  "attack" cmd-attack
  "use"  cmd-use
  "say"  cmd-say
})

(var ALIASES {
  "q" "quit"  "exit" "quit"
  "i" "inventory"  "inv" "inventory"  "invent" "inventory"
  "l" "look"

  "dive" "swim"
  "take" "get"
  "offer" "give"
  "unlock" "open"
  "kill" "attack"  "hit" "attack"  "fight" "attack"
  "apply" "use"
  "speak" "say"  "tell" "say"

  "north" "n"
  "south" "s"
  "east" "e"
  "west" "w"
  "up" "u"
  "down" "d"
})


;; Parsing Utils ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(fun lowercase-words! (words)
  (map! words (lam (w) (lowercase w)))
)

(fun split-words (line)
  (var words [])
  (var curword "")

  (var add-char (lam (ch)
    (set! curword (curword + (char$ ch)))
  ))

  (var add-word (lam ()
    (if (curword != "")
      (append! words curword)
    )
    (set! curword "")
  ))

  (var process-char (lam (ch)
    (if (char-space? ch)
      (add-word)
    else
      (add-char ch)
    )
  ))

  (for-each _ ch line (process-char ch))

  ;; ensure we have the last word
  (add-word)

  words
)

(var DUMMY_WORDS {
  "a" TRUE
  "an" TRUE
  "the" TRUE
  "to" TRUE
  "with" TRUE
})

(fun valid-word? (w)
  (not (DUMMY_WORDS w))
)

(var ABBREVIATIONS {
  "croc" "crocodile"
})

(fun expand-abbr! (words)
  (map! words (lam (w)
    (var abbr (ABBREVIATIONS w))
    (if (abbr != NIL) abbr else w)
  ))
)

;;--- editor settings ---
;; vi:ts=2:sw=2:expandtab
