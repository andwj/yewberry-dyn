// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"

type Operation struct {
	T OpCode // Type
	S int16  // Source
	M int16  // Misc
	D int16  // Destination

	LineNum int
}

const NO_PART int16 = -32767

type OpCode int16

const (
	OP_INVALID OpCode = iota

	/* Control Flow */

	OP_JUMP      //  PC := D
	OP_IF_FALSE  //  if S == NIL then PC := D
	OP_IF_TRUE   //  if S != NIL then PC := D
	OP_FUN_CALL  //  call function,  S = stack frame, M = num params
	OP_TAIL_CALL //  tail-call func, S = stack frame, M = num params
	OP_RETURN    //  return from current function
	OP_HOIST_VAR //  S is offset of var

	OP_FOR_BEGIN  //  S[2] := calc step to go from S to S[1]
	OP_FOR_LOOP   //  S := S + S[2], if S <= S[1] then PC := D
	OP_EACH_BEGIN //  store initial values in S[0..1], if empty then PC := D
	OP_EACH_LOOP  //  store next values in S[0..1], if no more then PC := D

	/* Stack Stuff */

	OP_NOP   //  does nothing
	OP_COPY  //  D := S
	OP_TOSTR //  D := S converted to a string

	/* Memory Access */

	OP_GLOB_READ  //  D := globals[S]
	OP_GLOB_WRITE //  globals[D] := S
	OP_UPV_READ   //  D := upvalues[S]
	OP_UPV_WRITE  //  upvalues[D] := S
	OP_ELEM_READ  //  D := S[M]
	OP_ELEM_WRITE //  D[M] := S
	OP_APPEND     //  append S onto array D

	OP_MAKE_FUNC  //  D := new closure from template S
	OP_MAKE_ARRAY //  D := a freshly allocated array
	OP_MAKE_MAP   //  D := a freshly allocated map
)

//----------------------------------------------------------------------

// NOTE: we never clear the data stack, e.g. after a function
// returns, hence the Go garbage collector may keep stuff
// alive longer than necessary (potentially indefinitely).

const MAX_DATA_STACK = 250000
const MAX_CALL_STACK = 25000

var data_stack [MAX_DATA_STACK]Value
var call_stack [MAX_CALL_STACK]FuncCallState

var call_p int
var call_cur *FuncCallState

// when this is not NIL, an run-time error has occurred
var run_error error

type FuncCallState struct {
	// the function being run
	cl *Closure

	// base of stack frame (SP) for this function
	data_p int

	// the code pointer (PC) for this function
	op_ptr int

	// all the local vars captured by lambda functions.
	// there is never more than one Upvalue here for the same
	// local variable.
	upvalues []*Upvalue

	// this is used to detect run-away loops
	iterations int32
}

//----------------------------------------------------------------------

func Die(msg string, a ...interface{}) {
	// determine location of the error
	var lno int
	var funcname string

	if call_p >= 1 {
		csf := &call_stack[call_p-1]

		if call_p >= 2 && csf.cl.builtin != nil {
			csf = &call_stack[call_p-2]
		}

		lno = csf.DetermineLine()
		funcname = csf.cl.debug_name
	}

	run_error = LocErrorf(lno, msg, a...)

	if funcname != "" {
		run_error = LocErrorWithFunc(run_error, funcname)
	}

	// show a stack trace
	trace_g := global_lookup["STACK-TRACE"]
	trace_size := globals[trace_g].loc.Int

	CallStackTrace(int(trace_size))
}

func SetResult(v Value) {
	data_stack[call_cur.data_p] = v
}

func SetResultBool(b bool) {
	data_stack[call_cur.data_p].MakeBool(b)
}

func SetResultInt(i int32) {
	data_stack[call_cur.data_p].MakeInt(i)
}

func SetResultString(s string) {
	data_stack[call_cur.data_p].MakeString(s)
}

func SetResultNIL() {
	data_stack[call_cur.data_p].MakeNIL()
}

//----------------------------------------------------------------------

func RunTop(cl *Closure) (Value, error) {
	// reset call stack
	call_p = 0

	// reset error condition
	run_error = nil

	if !Run(cl, 0) {
		// an error occurred
		return Value{}, run_error
	}

	return data_stack[0], Ok
}

func RunChild(cl *Closure) (Value, error) {
	data_p := ChildDataFrame()

	if !Run(cl, data_p) {
		// an error occurred
		return Value{}, run_error
	}

	return data_stack[data_p], Ok
}

func ChildDataFrame() int {
	return call_cur.data_p + int(call_cur.cl.high_water) + 2
}

// Run executes the compiled code in the closure.
// Parameters must be setup earlier (in the data stack).
// Returns true if ok, false if an error was raised.
func Run(cl *Closure, data_p int) bool {
	start_p := call_p

	if !CallStackPush(cl, data_p) {
		return false
	}

	if cl.builtin != nil {
		cl.builtin.code(data_p)
		CallStackPop()
		return run_error == nil
	}

	max_loop_idx := global_lookup["MAX-LOOP"]
	max_loop := globals[max_loop_idx].loc.Int

	for call_p > start_p {
		call_cur = &call_stack[call_p-1]

		for call_cur.op_ptr < len(call_cur.cl.vm_ops) {
			op := &call_cur.cl.vm_ops[call_cur.op_ptr]
			call_cur.op_ptr += 1

			if op.T == OP_RETURN {
				break
			}

			PerformOp(op, call_cur.cl)

			if run_error != nil {
				return false
			}

			call_cur.iterations++

			if call_cur.iterations >= max_loop {
				Die("run-away loop detected")
				return false
			}
		}

		CallStackPop()
	}

	return true
}

func CallStackPush(cl *Closure, data_p int) bool {
	// check for stack overflow
	if call_p+2 >= len(call_stack) {
		Die("overflow of call stack")
		return false
	}

	if data_p+int(cl.high_water)+10 >= len(data_stack) {
		Die("overflow of data stack")
		return false
	}

	call_p++

	call_cur = &call_stack[call_p-1]

	call_cur.cl = cl
	call_cur.data_p = data_p
	call_cur.op_ptr = 0
	call_cur.iterations = 0

	return true
}

func CallStackPop() {
	call_cur.cl = nil
	call_cur.upvalues = nil

	call_p--

	if call_p > 0 {
		call_cur = &call_stack[call_p-1]
	}
}

func (csf *FuncCallState) DetermineLine() int {
	if csf.cl.builtin != nil {
		return 0
	}

	op_ptr := csf.op_ptr - 1

	if op_ptr >= 0 && op_ptr < len(csf.cl.vm_ops) {
		return csf.cl.vm_ops[op_ptr].LineNum
	}

	return 0
}

func CallStackTrace(size int) {
	if size <= 0 {
		return
	}

	fmt.Printf("Stack trace:\n")

	last_p := call_p - 1 - size
	if last_p < 0 {
		last_p = 0
	}

	for i := call_p - 1; i >= last_p; i-- {
		csf := &call_stack[i]
		lno := csf.DetermineLine()

		if lno <= 0 {
			fmt.Printf("   [%03d] %s\n", i, csf.cl.debug_name)
		} else {
			fmt.Printf("   [%03d] %s (line %d)\n", i, csf.cl.debug_name, lno)
		}
	}
}

func PerformOp(op *Operation, cl *Closure) {
	switch op.T {
	case OP_JUMP:
		perform_Jump(op, cl)

	case OP_IF_FALSE:
		perform_IfFalse(op, cl)

	case OP_IF_TRUE:
		perform_IfTrue(op, cl)

	case OP_FUN_CALL:
		perform_FunCall(op, cl)

	case OP_TAIL_CALL:
		perform_TailCall(op, cl)

	case OP_RETURN:
		// NOTE: handled by caller

	case OP_HOIST_VAR:
		perform_HoistVar(op, cl)

	case OP_FOR_BEGIN:
		perform_ForBegin(op, cl)

	case OP_FOR_LOOP:
		perform_ForLoop(op, cl)

	case OP_EACH_BEGIN:
		perform_EachBegin(op, cl)

	case OP_EACH_LOOP:
		perform_EachLoop(op, cl)

	/* Stack Stuff */

	case OP_NOP:
		// nothing to do

	case OP_COPY:
		perform_Copy(op, cl)

	case OP_TOSTR:
		perform_ToStr(op, cl)

	/* Memory Access */

	case OP_GLOB_READ:
		perform_GlobalRead(op, cl)

	case OP_GLOB_WRITE:
		perform_GlobalWrite(op, cl)

	case OP_UPV_READ:
		perform_UpvalRead(op, cl)

	case OP_UPV_WRITE:
		perform_UpvalWrite(op, cl)

	case OP_ELEM_READ:
		perform_ElemRead(op, cl)

	case OP_ELEM_WRITE:
		perform_ElemWrite(op, cl)

	case OP_APPEND:
		perform_Append(op, cl)

	case OP_MAKE_FUNC:
		perform_MakeFunc(op, cl)

	case OP_MAKE_ARRAY:
		perform_MakeArray(op, cl)

	case OP_MAKE_MAP:
		perform_MakeMap(op, cl)

	default:
		panic("UNKNOWN OPCODE FOUND")
	}
}

func perform_Copy(op *Operation, cl *Closure) {
	S := GetStackOrConstant(op.S, cl)

	data_stack[call_cur.data_p+int(op.D)] = S
}

func perform_ToStr(op *Operation, cl *Closure) {
	S := GetStackOrConstant(op.S, cl)

	if S.Kind != VAL_String {
		S.MakeString(S.DeepString())
	}

	data_stack[call_cur.data_p+int(op.D)] = S
}

func perform_GlobalRead(op *Operation, cl *Closure) {
	if op.S < 0 || int(op.S) >= len(globals) {
		panic("BAD GLOBAL-DEF INDEX")
	}

	S := globals[op.S].loc

	data_stack[call_cur.data_p+int(op.D)] = *S
}

func perform_GlobalWrite(op *Operation, cl *Closure) {
	if op.D < 0 || int(op.D) >= len(globals) {
		panic("BAD GLOBAL-DEF INDEX for WRITE")
	}

	S := GetStackOrConstant(op.S, cl)
	D := globals[op.D].loc

	*D = S
}

func perform_UpvalRead(op *Operation, cl *Closure) {
	if op.S < 0 || int(op.S) >= len(cl.upvalues) {
		panic("BAD UPVALUE INDEX")
	}

	upv := cl.upvalues[op.S]

	data_stack[call_cur.data_p+int(op.D)] = *upv.loc
}

func perform_UpvalWrite(op *Operation, cl *Closure) {
	if op.D < 0 || int(op.D) >= len(cl.upvalues) {
		panic("BAD UPVALUE INDEX for WRITE")
	}

	upv := cl.upvalues[op.D]

	S := GetStackOrConstant(op.S, cl)

	*upv.loc = S
}

func perform_HoistVar(op *Operation, cl *Closure) {
	offset := int(op.S)

	// find the var in the captured variables list.
	// it is OK if it does not exist -- that can happen when no
	// instantiated lambda functions referenced the variable
	// (hence no need for the variable to continue to exist).

	// NOTE: it is not necessary to remove the matching Upvalue
	//       struct the list -- it can never match again.

	for _, upv := range call_cur.upvalues {
		if upv.offset == offset {
			// copy current value
			upv.hoisted = data_stack[call_cur.data_p+offset]

			// update pointer
			upv.loc = &upv.hoisted

			// mark as hoisted
			upv.offset = -1
			break
		}
	}
}

func perform_Jump(op *Operation, cl *Closure) {
	if op.D < 0 || int(op.D) >= len(cl.vm_ops) {
		panic("BAD DESTINATION FOR OP_JUMP or OP_IF_XXX")
	}

	call_cur.op_ptr = int(op.D)
}

func perform_IfFalse(op *Operation, cl *Closure) {
	S := GetStackOrConstant(op.S, cl)

	if !S.IsTrue() {
		perform_Jump(op, cl)
	}
}

func perform_IfTrue(op *Operation, cl *Closure) {
	S := GetStackOrConstant(op.S, cl)

	if S.IsTrue() {
		perform_Jump(op, cl)
	}
}

func perform_ForBegin(op *Operation, cl *Closure) {
	// calculate the step value: -1 or +1

	data_p := call_cur.data_p + int(op.S)

	start := data_stack[data_p]
	target := data_stack[data_p+1]

	if start.Kind != VAL_Int || target.Kind != VAL_Int {
		Die("for loop requires integer start and end")
		return
	}

	if start.Int > target.Int {
		data_stack[data_p+2].MakeInt(-1)
	} else {
		data_stack[data_p+2].MakeInt(1)
	}
}

func perform_ForLoop(op *Operation, cl *Closure) {
	data_p := call_cur.data_p + int(op.S)

	target := data_stack[data_p+1].Int
	step := data_stack[data_p+2].Int

	if data_stack[data_p].Int == target {
		// reached end, so just stop
		return
	}

	// add step to current value and loop
	data_stack[data_p].Int += step

	perform_Jump(op, cl)
}

func perform_EachBegin(op *Operation, cl *Closure) {
	data_p := call_cur.data_p + int(op.S)

	idx := &data_stack[data_p]
	val := &data_stack[data_p+1]
	arr := &data_stack[data_p+2]
	work := &data_stack[data_p+3]

	switch arr.Kind {
	case VAL_Array:
		// jump if empty
		if len(arr.Array.data) == 0 {
			perform_Jump(op, cl)
			return
		}

		// set index and value vars
		idx.MakeInt(0)
		*val = arr.GetElem(int(idx.Int))

	case VAL_String:
		// jump if empty
		r := []rune(arr.Str)
		if len(r) == 0 {
			perform_Jump(op, cl)
			return
		}

		// set index and value vars
		idx.MakeInt(0)
		val.MakeInt(int32(r[idx.Int]))

	case VAL_Map:
		// collect all the keys into a list.
		// we work our way *backwards* through this list.
		*work = arr.CollectKeys()

		// jump if empty
		length := work.Length()

		if length == 0 {
			perform_Jump(op, cl)
			return
		}

		// set index and value vars
		*idx = work.GetElem(length - 1)
		*val = arr.KeyGet(idx.Str)

		// remove last key from the list
		work.DeleteElem(length - 1)

	default:
		Die("for-each requires an array/map/string")
		return
	}
}

func perform_EachLoop(op *Operation, cl *Closure) {
	data_p := call_cur.data_p + int(op.S)

	idx := &data_stack[data_p]
	val := &data_stack[data_p+1]
	arr := &data_stack[data_p+2]
	work := &data_stack[data_p+3]

	switch arr.Kind {
	case VAL_Array:
		// update index var
		idx.Int += 1

		// stop if nothing left
		if int(idx.Int) >= len(arr.Array.data) {
			return
		}

		// update value var
		*val = arr.GetElem(int(idx.Int))

		// jump back to the body
		perform_Jump(op, cl)

	case VAL_String:
		// update index var
		idx.Int += 1

		// NOTE: we do this conversion for every char, which is far from
		// ideal.  A faster alternative is to create an array of integers
		// in the OP_EACH_BEGIN code.  Not sure if it's worth it though.
		r := []rune(arr.Str)

		// stop if nothing left
		if int(idx.Int) >= len(r) {
			return
		}

		// update value var
		val.MakeInt(int32(r[idx.Int]))

		// jump back to the body
		perform_Jump(op, cl)

	case VAL_Map:
		length := work.Length()

		// stop if nothing left
		if length == 0 {
			return
		}

		// update index and value vars
		*idx = work.GetElem(length - 1)
		*val = arr.KeyGet(idx.Str)

		// remove last key from the list
		work.DeleteElem(length - 1)

		// jump back to the body
		perform_Jump(op, cl)

	default:
		Die("for-each requires an array/map/string")
		return
	}
}

func perform_FunCall(op *Operation, cl *Closure) {
	F := GetStackOrConstant(op.S, cl)

	// we also support accessing a container
	switch F.Kind {
	case VAL_Array, VAL_Map, VAL_String:
		perform_AccessObj(op, cl, F)
		return
	}

	if F.Kind != VAL_Function {
		Die("cannot call/access a %s", F.Kind.String())
		return
	}

	new_cl := F.Clos
	data_p := call_cur.data_p + int(op.S)
	num_pars := int(op.M)

	// handle built-in primitives
	if new_cl.builtin != nil {
		if num_pars != new_cl.builtin.args {
			Die("wrong number of parameters, wanted: %d, got: %d",
				new_cl.builtin.args, num_pars)
			return
		}

		if CallStackPush(new_cl, data_p) {
			new_cl.builtin.code(data_p)
			CallStackPop()
		}

		return
	}

	// check # of parameters
	if num_pars != len(new_cl.parameters) {
		Die("wrong number of parameters, wanted: %d, got: %d",
			len(new_cl.parameters), num_pars)
		return
	}

	if !CallStackPush(new_cl, data_p) {
		// stack overflow error
		return
	}

	// run the code normally, see Run() above
}

func perform_TailCall(op *Operation, cl *Closure) {
	F := GetStackOrConstant(op.S, cl)

	// for builtins or container access, treat as a normal call
	if F.Kind != VAL_Function || F.Clos.builtin != nil {
		perform_FunCall(op, cl)

		// simulate the OP_COPY that occurs after a normal function call
		data_stack[call_cur.data_p] = data_stack[call_cur.data_p+int(op.S)]
		return
	}

	new_cl := F.Clos

	// check # of parameters
	num_pars := int(op.M)

	if num_pars != len(new_cl.parameters) {
		Die("wrong number of parameters, wanted: %d, got: %d",
			len(new_cl.parameters), num_pars)
		return
	}

	// firstly, execute the last operations of the current
	// function.  by the very nature of a tail call, there
	// will NOT be any computation or func calls after it,
	// only a very limited set of operations.
	//
	// we do this to ensure captured variables get hoisted
	// to the heap.

	for call_cur.op_ptr < len(call_cur.cl.vm_ops) {
		op2 := &call_cur.cl.vm_ops[call_cur.op_ptr]
		call_cur.op_ptr += 1

		if op2.T == OP_RETURN {
			break
		}

		switch op2.T {
		case OP_NOP, OP_JUMP, OP_HOIST_VAR:
			PerformOp(op2, call_cur.cl)
		default:
			panic("illegal operation after tail-call: " + op2.OpString())
		}
	}

	save_iter := call_cur.iterations

	// rejig data stack: copy down the parameters
	data_p := call_cur.data_p

	frame := int(op.S)

	for i := 0; i < num_pars; i++ {
		data_stack[data_p+1+i] = data_stack[data_p+frame+1+i]
	}

	// rejig the call stack
	CallStackPop()
	CallStackPush(new_cl, data_p)

	// keep iteration counter, to detect run-away recursive loops
	call_cur.iterations = save_iter
}

func perform_AccessObj(op *Operation, cl *Closure, v Value) {
	if op.M < 1 {
		Die("read element: missing index/key")
		return
	}

	// normally a function call leaves a value at the base of its
	// stack frame, so simulate that here.

	dest_ofs := call_cur.data_p + int(op.S)
	dest := &data_stack[dest_ofs]

	for p := 0; p < int(op.M); p++ {
		idx := data_stack[call_cur.data_p+int(op.S)+1+p]

		switch v.Kind {
		case VAL_Array:
			if idx.Kind != VAL_Int {
				Die("read array element: index must be integer")
				return
			}

			// result is NIL if *any* dimension is out of bounds
			if !v.ValidIndex(int(idx.Int)) {
				dest.MakeNIL()
				return
			}

			v = v.GetElem(int(idx.Int))

		case VAL_Map:
			if idx.Kind != VAL_String {
				Die("read map element: key must be a string")
				return
			}

			v = v.KeyGet(idx.Str)

		case VAL_String:
			if idx.Kind != VAL_Int {
				Die("read string element: index must be integer")
				return
			}

			r := []rune(v.Str)

			if idx.Int < 0 || int(idx.Int) >= len(r) {
				dest.MakeNIL()
				return
			}

			v.MakeInt(int32(r[idx.Int]))

		default:
			if v.IsNIL() {
				Die("read element: cannot access a NIL object")
			} else {
				Die("read element: object must be array/map/string")
			}
			return
		}
	}

	*dest = v
}

func perform_ElemRead(op *Operation, cl *Closure) {
	arr := GetStackOrConstant(op.S, cl)
	idx := GetStackOrConstant(op.M, cl)

	dest := &data_stack[call_cur.data_p+int(op.D)]

	switch arr.Kind {
	case VAL_Array:
		if idx.Kind != VAL_Int {
			Die("read array element: index must be integer")
			return
		}

		// result is NIL if *any* dimension is out of bounds
		if !arr.ValidIndex(int(idx.Int)) {
			SetResultNIL()
			return
		}

		*dest = arr.GetElem(int(idx.Int))

	case VAL_Map:
		if idx.Kind != VAL_String {
			Die("read map element: key must be a string")
			return
		}

		*dest = arr.KeyGet(idx.Str)

	case VAL_String:
		if idx.Kind != VAL_Int {
			Die("read string element: index must be integer")
			return
		}

		r := []rune(arr.Str)

		if idx.Int < 0 || int(idx.Int) >= len(r) {
			SetResultNIL()
			return
		}

		dest.MakeInt(int32(r[idx.Int]))

	default:
		Die("read element: object must be array/map/string")
		return
	}
}

func perform_ElemWrite(op *Operation, cl *Closure) {
	val := GetStackOrConstant(op.S, cl)
	idx := GetStackOrConstant(op.M, cl)
	arr := data_stack[call_cur.data_p+int(op.D)]

	switch arr.Kind {
	case VAL_Array:
		if idx.Kind != VAL_Int {
			Die("write array element: index must be an integer")
			return
		}
		if !arr.ValidIndex(int(idx.Int)) {
			Die("write array element: index out of bounds")
			return
		}
		arr.SetElem(int(idx.Int), val)

	case VAL_Map:
		if idx.Kind != VAL_String {
			Die("write map element: key must be a string")
			return
		}
		arr.KeySet(idx.Str, val)

	default:
		Die("can only set elements of array or map")
		return
	}
}

func perform_Append(op *Operation, cl *Closure) {
	S := GetStackOrConstant(op.S, cl)
	D := data_stack[call_cur.data_p+int(op.D)]

	switch D.Kind {
	case VAL_String:
		if S.Kind != VAL_String {
			Die("cannot append %s onto a string", S.Kind.String())
			return
		}
		dest := &data_stack[call_cur.data_p+int(op.D)]
		dest.MakeString(D.Str + S.Str)

	case VAL_Array:
		D.AppendElem(S)

	default:
		Die("cannot append onto a %s", D.Kind.String())
		return
	}
}

func perform_MakeFunc(op *Operation, cl *Closure) {
	S := GetStackOrConstant(op.S, cl)
	template := S.Clos

	newcl := new(Closure)
	newcl.vm_ops = template.vm_ops
	newcl.parameters = template.parameters
	newcl.constants = template.constants
	newcl.debug_name = template.debug_name + fmt.Sprintf("@%p", newcl)

	// create upvalues
	newcl.upvalues = make([]*Upvalue, 0)

	for _, lvar := range template.captures {
		upv := CreateUpvalue(lvar)

		newcl.upvalues = append(newcl.upvalues, upv)
	}

	data_stack[call_cur.data_p+int(op.D)].MakeFunction(newcl)
}

func perform_MakeArray(op *Operation, cl *Closure) {
	data_stack[call_cur.data_p+int(op.D)].MakeArray(0)
}

func perform_MakeMap(op *Operation, cl *Closure) {
	data_stack[call_cur.data_p+int(op.D)].MakeMap()
}

func GetStackOrConstant(ofs int16, cl *Closure) Value {
	if ofs < 0 {
		// a constant index
		ofs = -ofs - 1
		if int(ofs) >= len(cl.constants) {
			panic("BAD CONSTANT INDEX")
		}
		return cl.constants[ofs]
	}

	return data_stack[call_cur.data_p+int(ofs)]
}

func CreateUpvalue(lvar *LocalVar) *Upvalue {
	// here we capture a local variable as an Upvalue struct.
	// a local variable can only be captured *once*, hence we
	// need to see if it has already been created (by another
	// instantiated lambda function).

	// when the local variable does not belong to the currently
	// executing function, then the lambda we are instantiating
	// must be a sub-lambda (i.e. a lambda within a lambda) and
	// hence the parent lambda will have captured this variable
	// already -- the CaptureLocal() code guarantees this.
	if lvar.owner != call_cur.cl {
		for _, upv := range call_cur.cl.upvalues {
			if upv.lvar == lvar {
				return upv
			}
		}

		// this should not happen
		panic("CANNOT FIND UP-UP-VALUE: " + lvar.name)
	}

	// the variable is in the current stack frame.
	// has it been captured already?
	for _, upv := range call_cur.upvalues {
		if upv.lvar == lvar {
			return upv
		}
	}

	data_p := call_cur.data_p + int(lvar.offset)

	// capture it now
	upv := new(Upvalue)
	upv.lvar = lvar
	upv.offset = int(lvar.offset)
	upv.loc = &data_stack[data_p]

	call_cur.upvalues = append(call_cur.upvalues, upv)

	return upv
}

//----------------------------------------------------------------------

func DumpOps(cl *Closure) {
	fmt.Printf("vm_ops for %s\n", cl.debug_name)
	for idx, op := range cl.vm_ops {
		fmt.Printf("   [%03d] %s\n", idx, op.String(cl))
	}
}

func (op *Operation) String(cl *Closure) string {
	S := op.PartString(cl, "S", op.S)
	M := op.PartString(cl, "M", op.M)
	D := op.PartString(cl, "D", op.D)

	return fmt.Sprintf("%-12s  %-5s  %-5s  %-5s", op.OpString(), S, D, M)
}

func (op *Operation) OpString() string {
	switch op.T {
	case OP_JUMP:
		return "OP_JUMP"
	case OP_IF_FALSE:
		return "OP_IF_FALSE"
	case OP_IF_TRUE:
		return "OP_IF_TRUE"
	case OP_FUN_CALL:
		return "OP_FUN_CALL"
	case OP_TAIL_CALL:
		return "OP_TAIL_CALL"
	case OP_RETURN:
		return "OP_RETURN"
	case OP_HOIST_VAR:
		return "OP_HOIST_VAR"

	case OP_FOR_BEGIN:
		return "OP_FOR_BEGIN"
	case OP_FOR_LOOP:
		return "OP_FOR_LOOP"
	case OP_EACH_BEGIN:
		return "OP_EACH_BEGIN"
	case OP_EACH_LOOP:
		return "OP_EACH_LOOP"

	case OP_NOP:
		return "OP_NOP"
	case OP_COPY:
		return "OP_COPY"
	case OP_TOSTR:
		return "OP_TOSTR"

	case OP_GLOB_READ:
		return "OP_GLOB_READ"
	case OP_GLOB_WRITE:
		return "OP_GLOB_WRITE"
	case OP_UPV_READ:
		return "OP_UPV_READ"
	case OP_UPV_WRITE:
		return "OP_UPV_WRITE"
	case OP_ELEM_READ:
		return "OP_ELEM_READ"
	case OP_ELEM_WRITE:
		return "OP_ELEM_WRITE"
	case OP_APPEND:
		return "OP_APPEND"

	case OP_MAKE_FUNC:
		return "OP_MAKE_FUNC"
	case OP_MAKE_ARRAY:
		return "OP_MAKE_ARRAY"
	case OP_MAKE_MAP:
		return "OP_MAKE_MAP"

	default:
		return "?????"
	}
}

func (op *Operation) PartString(cl *Closure, name string, val int16) string {
	if val == NO_PART {
		return ""
	}

	// handle the constants block
	if val < 0 && int(-val-1) < len(cl.constants) {
		v := cl.constants[-val-1]
		if v.Kind == VAL_Int || v.Kind == VAL_String {
			return fmt.Sprintf("%s=%v (%s)", name, val, v.DeepString())
		}
	}

	// handle global reads
	if op.T == OP_GLOB_READ && name == "S" {
		def := globals[op.S]
		return fmt.Sprintf("%s=%v (%s)", name, val, def.name)
	}

	return fmt.Sprintf("%s=%v", name, val)
}
