// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "io"
import "os"
import "strings"
import "math/rand"
import "unicode"

import "github.com/peterh/liner"

type Builtin struct {
	name string
	args int
	code func(int)
}

var did_print bool

func SetupBuiltins() {
	// boolean logic
	RegisterBuiltin("and", 2, BI_And)
	RegisterBuiltin("or", 2, BI_Or)
	RegisterBuiltin("not", 1, BI_Not)

	// comparisons
	RegisterBuiltin("eq?", 2, BI_Equal)
	RegisterBuiltin("lt?", 2, BI_Less)
	RegisterBuiltin("gt?", 2, BI_Greater)
	RegisterBuiltin("ne?", 2, BI_NotEqual)
	RegisterBuiltin("le?", 2, BI_LessEq)
	RegisterBuiltin("ge?", 2, BI_GreaterEq)

	RegisterBuiltin("same-obj?", 2, BI_SameObj)

	// string manipulation
	RegisterBuiltin("hex$", 1, BI_HexToStr)
	RegisterBuiltin("char$", 1, BI_CharToStr)
	RegisterBuiltin("quote$", 1, BI_QuotedStr)
	RegisterBuiltin("parse", 1, BI_Parse)
	RegisterBuiltin("lowercase", 1, BI_Lowercase)
	RegisterBuiltin("uppercase", 1, BI_Uppercase)

	// character tests
	RegisterBuiltin("char-letter?", 1, BI_CharLetter)
	RegisterBuiltin("char-digit?", 1, BI_CharDigit)
	RegisterBuiltin("char-symbol?", 1, BI_CharSymbol)
	RegisterBuiltin("char-mark?", 1, BI_CharMark)
	RegisterBuiltin("char-space?", 1, BI_CharSpace)
	RegisterBuiltin("char-control?", 1, BI_CharControl)

	// array and map functions
	RegisterBuiltin("len", 1, BI_Len)
	RegisterBuiltin("empty?", 1, BI_Empty)
	RegisterBuiltin("copy", 1, BI_Copy)
	RegisterBuiltin("subseq", 3, BI_Subseq)

	RegisterBuiltin("append!", 2, BI_Append)
	RegisterBuiltin("insert!", 3, BI_Insert)
	RegisterBuiltin("delete!", 2, BI_Delete)
	RegisterBuiltin("join!", 2, BI_Join)

	RegisterBuiltin("map!", 2, BI_Map)
	RegisterBuiltin("filter!", 2, BI_Filter)
	RegisterBuiltin("sort!", 2, BI_Sort)
	RegisterBuiltin("find", 2, BI_Find)
	RegisterBuiltin("has?", 2, BI_HasElem)

	// integer operations
	RegisterBuiltin("add", 2, BI_Add)
	RegisterBuiltin("sub", 2, BI_Subtract)
	RegisterBuiltin("mul", 2, BI_Multiply)
	RegisterBuiltin("div", 2, BI_Divide)
	RegisterBuiltin("mod", 2, BI_Modulo)
	RegisterBuiltin("pow", 2, BI_Power)

	RegisterBuiltin("abs", 1, BI_Abs)
	RegisterBuiltin("min", 2, BI_Min)
	RegisterBuiltin("max", 2, BI_Max)

	// bitwise operations
	RegisterBuiltin("bit-or", 2, BI_BitOr)
	RegisterBuiltin("bit-and", 2, BI_BitAnd)
	RegisterBuiltin("bit-xor", 2, BI_BitXor)
	RegisterBuiltin("bit-not", 1, BI_BitNot)

	RegisterBuiltin("shift-left", 2, BI_ShiftLeft)
	RegisterBuiltin("shift-right", 2, BI_ShiftRight)

	// type checking
	RegisterBuiltin("type-int?", 1, BI_TypeInt)
	RegisterBuiltin("type-string?", 1, BI_TypeString)
	RegisterBuiltin("type-array?", 1, BI_TypeArray)
	RegisterBuiltin("type-map?", 1, BI_TypeMap)
	RegisterBuiltin("type-fun?", 1, BI_TypeFunction)

	// random numbers
	RegisterBuiltin("rand-seed", 1, BI_RandSeed)
	RegisterBuiltin("rand-int", 1, BI_RandInt)
	RegisterBuiltin("rand-range", 2, BI_RandRange)

	// I/O and misc
	RegisterBuiltin("error", 1, BI_Error)
	RegisterBuiltin("print", 1, BI_Print)
	RegisterBuiltin("input", 1, BI_Input)
	RegisterBuiltin("enable-editor", 0, BI_EnableEditor)
}

func RegisterBuiltin(name string, args int, code func(data_p int)) {
	cl := new(Closure)
	cl.builtin = &Builtin{name, args, code}
	cl.debug_name = name

	MakeGlobal(name).loc.MakeFunction(cl)
}

//----------------------------------------------------------------------

type OperatorInfo struct {
	name   string
	actual *GlobalDef

	unary       bool
	precedence  int
	right_assoc bool
}

var operators map[string]*OperatorInfo

func SetupOperators() {
	operators = make(map[string]*OperatorInfo)

	// these two are unary operators
	RegisterOperator(0, "!", "not")
	RegisterOperator(0, "~", "bit-not")

	RegisterOperator(1, "||", "or")
	RegisterOperator(2, "&&", "and")

	RegisterOperator(3, "==", "eq?")
	RegisterOperator(3, "!=", "ne?")
	RegisterOperator(4, "<", "lt?")
	RegisterOperator(4, ">", "gt?")
	RegisterOperator(4, "<=", "le?")
	RegisterOperator(4, ">=", "ge?")

	RegisterOperator(5, "+", "add")
	RegisterOperator(5, "-", "sub")
	RegisterOperator(6, "*", "mul")
	RegisterOperator(6, "/", "div")
	RegisterOperator(6, "%", "mod")

	RegisterOperator(5, "|", "bit-or")
	RegisterOperator(5, "^", "bit-xor")
	RegisterOperator(6, "&", "bit-and")

	RegisterOperator(6, "<<", "shift-left")
	RegisterOperator(6, ">>", "shift-right")
	RegisterOperator(7, "**", "pow")
}

func RegisterOperator(prec int, op string, actual string) {
	idx, exist := global_lookup[actual]
	if !exist {
		panic("unknown builtin '" + actual + "' for operator")
	}

	def := globals[idx]
	loc := def.loc

	if loc.Kind != VAL_Function || loc.Clos == nil || loc.Clos.builtin == nil {
		panic("invalid builtin '" + actual + "' for operator")
	}

	info := new(OperatorInfo)

	info.name = op
	info.actual = def

	info.unary = (prec == 0)
	info.precedence = prec
	info.right_assoc = (op == "**")

	operators[op] = info
}

func HasOperator(t *Token) bool {
	for _, child := range t.Children {
		if child.Kind == TOK_Name {
			if operators[child.Str] != nil {
				return true
			}
		}
	}
	return false
}

//----------------------------------------------------------------------

func BI_And(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	SetResultBool(a.IsTrue() && b.IsTrue())
}

func BI_Or(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	SetResultBool(a.IsTrue() || b.IsTrue())
}

func BI_Not(data_p int) {
	v := data_stack[data_p+1]

	SetResultBool(!v.IsTrue())
}

func BI_Equal(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	equal := BI_raw_equality(a, b)

	SetResultBool(equal)
}

func BI_NotEqual(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	equal := BI_raw_equality(a, b)

	SetResultBool(!equal)
}

func BI_raw_equality(a, b Value) bool {
	if a.Kind == b.Kind {
		switch a.Kind {
		case VAL_Int:
			return a.Int == b.Int

		case VAL_String:
			return a.Str == b.Str

		case VAL_Array:
			Die("cannot compare two arrays")

		case VAL_Map:
			Die("cannot compare two maps")

		case VAL_Function:
			Die("cannot compare two functions")
		}
	}

	return false
}

func BI_Less(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	cmp := BI_raw_compare(a, b)

	SetResultBool(cmp < 0)
}

func BI_LessEq(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	cmp := BI_raw_compare(a, b)

	SetResultBool(cmp <= 0)
}

func BI_Greater(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	cmp := BI_raw_compare(a, b)

	SetResultBool(cmp > 0)
}

func BI_GreaterEq(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	cmp := BI_raw_compare(a, b)

	SetResultBool(cmp >= 0)
}

func BI_raw_compare(a, b Value) int {
	if a.Kind != b.Kind {
		Die("cannot compare order of different types")
		return 0
	}

	switch a.Kind {
	case VAL_Int:
		switch {
		case a.Int < b.Int:
			return -1
		case a.Int > b.Int:
			return 1
		default:
			return 0
		}

	case VAL_String:
		switch {
		case a.Str < b.Str:
			return -1
		case a.Str > b.Str:
			return 1
		default:
			return 0
		}

	case VAL_Array:
		Die("cannot compare order of arrays")

	case VAL_Map:
		Die("cannot compare order of maps")

	case VAL_Function:
		Die("cannot compare order of functions")
	}

	return 0
}

func BI_SameObj(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != b.Kind {
		SetResultNIL()
		return
	}

	switch a.Kind {
	case VAL_Array:
		SetResultBool(a.Array == b.Array)

	case VAL_Map:
		SetResultBool(a.Map == b.Map)

	case VAL_Function:
		SetResultBool(a.Clos == b.Clos)

	default:
		SetResultNIL()
	}
}

//----------------------------------------------------------------------

func BI_HexToStr(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("hex$ requires an integer")
		return
	}

	hexstr := fmt.Sprintf("0x%08X", uint32(v.Int))

	SetResultString(hexstr)
}

func BI_CharToStr(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char$ requires an integer")
		return
	}

	// NIL and negative values become the empty string
	if v.Int < 0 {
		SetResultString("")
		return
	}

	// convert character to a string
	var r [1]rune
	r[0] = rune(v.Int)

	SetResultString(string(r[:]))
}

func BI_QuotedStr(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_String {
		Die("quote$ requires a string")
		return
	}

	// we need to surround the string with double quotes, and
	// escape any strange characters (including '"' itself).
	// luckily the %q verb in the fmt package does all this.

	quoted := fmt.Sprintf("%q", v.Str)

	SetResultString(quoted)
}

func BI_Parse(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_String {
		Die("cannot parse a non-string")
		return
	}

	res := Value{}

	lex := NewLexer(strings.NewReader(v.Str))
	tok := lex.Parse()

	if tok.Kind == TOK_EOF {
		SetResult(*globals[GLOB_EOF].loc)
		return
	}
	if tok.Kind == TOK_ERROR {
		SetResultString("#ERROR# " + tok.Str)
		return
	}

	err := res.ParseLiteral(tok)
	if err != nil {
		SetResultString("#ERROR# " + err.Error())
		return
	}

	SetResult(res)
}

func BI_Lowercase(data_p int) {
	v := data_stack[data_p+1]

	res := Value{}

	switch v.Kind {
	case VAL_Int:
		ch := unicode.ToLower(rune(v.Int))
		res.MakeInt(int32(ch))

	case VAL_String:
		res.MakeString(strings.ToLower(v.Str))

	default:
		Die("lowercase primitive requires char or string")
		return
	}

	SetResult(res)
}

func BI_Uppercase(data_p int) {
	v := data_stack[data_p+1]

	res := Value{}

	switch v.Kind {
	case VAL_Int:
		ch := unicode.ToUpper(rune(v.Int))
		res.MakeInt(int32(ch))

	case VAL_String:
		res.MakeString(strings.ToUpper(v.Str))

	default:
		Die("uppercase primitive requires char or string")
		return
	}

	SetResult(res)
}

//----------------------------------------------------------------------

func BI_CharLetter(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char-letter? requires an integer")
		return
	}

	res := unicode.IsLetter(rune(v.Int))

	SetResultBool(res)
}

func BI_CharDigit(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char-digit? requires an integer")
		return
	}

	// Note: IsNumber() is another possibility here, but I am not
	// sure whether that would be correct or not.

	res := unicode.IsDigit(rune(v.Int))

	SetResultBool(res)
}

func BI_CharSymbol(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char-symbol? requires an integer")
		return
	}

	// Note: we treat P-class ("Punctuation") and S-class "Symbols"
	// the same here.  It is not really clear what the difference is.
	// e.g. Go considers '*' to be punctuation and '+' a symbol.
	// Also a single quote is punctuation, back-quote is a symbol.

	ch := rune(v.Int)

	res := unicode.IsSymbol(ch) || unicode.IsPunct(ch)

	SetResultBool(res)
}

func BI_CharMark(data_p int) {
	// Note: a "mark" is a combining character, like diacritics.
	// The unicode range U0300 to U036F are the main ones (but
	// there are others).

	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char-mark? requires an integer")
		return
	}

	res := unicode.IsMark(rune(v.Int))

	SetResultBool(res)
}

func BI_CharSpace(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char-space? requires an integer")
		return
	}

	res := unicode.IsSpace(rune(v.Int))

	SetResultBool(res)
}

func BI_CharControl(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("char-control? requires an integer")
		return
	}

	res := unicode.IsControl(rune(v.Int))

	SetResultBool(res)
}

//----------------------------------------------------------------------

func BI_Len(data_p int) {
	a := data_stack[data_p+1]

	SetResultInt(int32(a.Length()))
}

func BI_Empty(data_p int) {
	a := data_stack[data_p+1]

	SetResultBool(a.Length() == 0)
}

// append an element to an array.
// the array in first operand is copied to result afterwards,
// which the array literal compiling code relies on.
func BI_Append(data_p int) {
	arr := data_stack[data_p+1]
	val := data_stack[data_p+2]

	if arr.Kind != VAL_Array {
		Die("cannot append to a non-array")
		return
	}

	arr.AppendElem(val)

	SetResult(arr)
}

func BI_Insert(data_p int) {
	arr := data_stack[data_p+1]
	idx := data_stack[data_p+2]
	val := data_stack[data_p+3]

	if arr.Kind != VAL_Array {
		Die("can only insert into an array")
		return
	}

	if idx.Kind != VAL_Int {
		Die("insert array element: index must be integer")
		return
	}

	arr.InsertElem(int(idx.Int), val)

	SetResult(arr)
}

func BI_Delete(data_p int) {
	dest := data_stack[data_p+1]

	if !(dest.Kind == VAL_Array || dest.Kind == VAL_Map) {
		Die("can only delete in an array or map")
		return
	}

	v := data_stack[data_p+2]

	if dest.Kind == VAL_Array {
		if v.Kind != VAL_Int {
			Die("delete array element: index must be integer")
			return
		}
		dest.DeleteElem(int(v.Int))
	} else {
		if v.Kind != VAL_String {
			Die("delete map element: key must be a string")
			return
		}
		dest.KeyDelete(v.Str)
	}

	SetResult(dest)
}

func BI_Join(data_p int) {
	dest := data_stack[data_p+1]

	if !(dest.Kind == VAL_Array || dest.Kind == VAL_Map) {
		Die("can only join arrays or maps")
		return
	}

	v := data_stack[data_p+2]

	if v.Kind != dest.Kind {
		Die("incompatible type for join")
		return
	}
	if dest.Kind == VAL_Array && v.Array == dest.Array {
		Die("cannot join array with itself")
		return
	}
	if dest.Kind == VAL_Map && v.Map == dest.Map {
		// joining a map with itself is a no-op
		SetResult(dest)
		return
	}

	if dest.Kind == VAL_Array {
		for i := range v.Array.data {
			dest.AppendElem(v.GetElem(i))
		}
	} else {
		for k, ptr := range v.Map.data {
			dest.KeySet(k, *ptr)
		}
	}

	SetResult(dest)
}

func BI_Copy(data_p int) {
	v := data_stack[data_p+1]

	SetResult(v.ShallowCopy())
}

func BI_Subseq(data_p int) {
	arr := data_stack[data_p+1]

	if !(arr.Kind == VAL_Array || arr.Kind == VAL_String) {
		Die("can only use copy-part with array or string")
		return
	}

	// get current length, for string: convert to runes
	var r []rune
	var length int

	if arr.Kind == VAL_Array {
		length = len(arr.Array.data)
	} else {
		r = []rune(arr.Str)
		length = len(r)
	}

	// determine the start and end indices
	v1 := data_stack[data_p+2]
	v2 := data_stack[data_p+3]

	if v1.Kind != VAL_Int || v2.Kind != VAL_Int {
		Die("bad range for copy-part (not integers)")
		return
	}

	start := int(v1.Int)
	end := int(v2.Int)

	// clamp the start to 0, end to the length
	if start < 0 {
		start = 0
	}
	if end > length {
		end = length
	}

	res := Value{}

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if start >= end {
		if arr.Kind == VAL_Array {
			res.MakeArray(0)
		} else {
			res.MakeString("")
		}
		SetResult(res)
		return
	}

	// produce a shallow copy of the given range
	if arr.Kind == VAL_String {
		r = r[start:end]
		res.MakeString(string(r))
		SetResult(res)
	} else {
		res.MakeArray(end - start)
		copy(res.Array.data, arr.Array.data[start:end])
		SetResult(res)
	}
}

func BI_Map(data_p int) {
	dest := data_stack[data_p+1]

	if dest.Kind != VAL_Array {
		Die("can only use map! on arrays")
		return
	}

	pred := data_stack[data_p+2]

	if pred.Kind != VAL_Function {
		Die("predicate for map! must be a function, got %s",
			pred.Kind.String())
		return
	}

	if pred.NumParameters() != 1 {
		Die("predicate for map! must have a single parameter")
		return
	}

	for i := 0; i < dest.Length(); i++ {
		// push arguments to predicate
		child_p := ChildDataFrame()

		data_stack[child_p+1] = dest.GetElem(i)

		// run the predicate!
		res, err := RunChild(pred.Clos)
		if err != nil {
			return
		}

		dest.SetElem(i, res)
	}

	SetResult(dest)
}

func BI_Filter(data_p int) {
	dest := data_stack[data_p+1]

	if dest.Kind != VAL_Array {
		Die("can only use filter! on arrays")
		return
	}

	pred := data_stack[data_p+2]

	if pred.Kind != VAL_Function {
		Die("predicate for filter! must be a function, got %s",
			pred.Kind.String())
		return
	}

	if pred.NumParameters() != 1 {
		Die("predicate for filter! must have a single parameter")
		return
	}

	// it is essential to iterate *backwards* through the array
	for i := dest.Length() - 1; i >= 0; i-- {
		child_p := ChildDataFrame()

		data_stack[child_p+1] = dest.GetElem(i)

		// run the predicate!
		res, err := RunChild(pred.Clos)
		if err != nil {
			return
		}

		if !res.IsTrue() {
			dest.DeleteElem(i)
		}
	}

	SetResult(dest)
}

func BI_Find(data_p int) {
	pred := data_stack[data_p+2]

	if pred.Kind != VAL_Function {
		Die("predicate for find must be a function, got %s",
			pred.Kind.String())
		return
	}

	if pred.NumParameters() != 1 {
		Die("predicate for find must have a single parameter")
		return
	}

	arr := data_stack[data_p+1]

	if arr.Kind != VAL_Array {
		Die("can only use find on arrays")
		return
	}

	for i := range arr.Array.data {
		child_p := ChildDataFrame()

		data_stack[child_p+1] = arr.GetElem(i)

		// run the predicate!
		res, err := RunChild(pred.Clos)
		if err != nil {
			return
		}

		// found the wanted element?
		if res.IsTrue() {
			SetResultInt(int32(i))
			return
		}
	}

	// not found
	SetResultNIL()
}

func BI_HasElem(data_p int) {
	m := data_stack[data_p+1]
	key := data_stack[data_p+2]

	if m.Kind != VAL_Map {
		Die("can only use has? on maps")
		return
	}

	if key.Kind != VAL_String {
		Die("has? requires a string as key")
	}

	res := m.KeyExists(key.Str)

	SetResultBool(res)
}

func BI_Sort(data_p int) {
	dest := data_stack[data_p+1]
	pred := data_stack[data_p+2]

	if dest.Kind != VAL_Array {
		Die("can only use sort! on arrays")
		return
	}

	if pred.Kind != VAL_Function {
		Die("predicate for sort! must be a function, got %s",
			pred.Kind.String())
		return
	}

	if pred.NumParameters() != 2 {
		Die("predicate for sort! must have two parameters")
		return
	}

	comparator := func(i, k int) bool {
		// check if an run-time error has occurred
		if run_error != nil {
			return false
		}

		child_p := ChildDataFrame()

		data_stack[child_p+1] = dest.Array.data[i]
		data_stack[child_p+2] = dest.Array.data[k]

		// run the predicate!
		res, err := RunChild(pred.Clos)
		if err != nil {
			return false
		}

		return res.IsTrue()
	}

	// perform a preliminary pass over the array and run the
	// predicate on neighboring pairs.  This is to catch obvious
	// errors early, e.g. array contains wrong type of elements.

	for i := dest.Length() - 2; i >= 0; i-- {
		comparator(i, i+1)

		if run_error != nil {
			return
		}
	}

	if dest.Length() < 2 {
		SetResult(dest)
		return
	}

	safesort_Array(&dest, 0, dest.Length()-1, comparator)

	SetResult(dest)
}

func safesort_Array(arr *Value, s, e int, cmp func(i, k int) bool) {
	// this is a Quick-sort algorithm, but crafted so that the
	// comparison function may return complete garbage and this
	// code will not crash (e.g. access out-of-bound elements).

	// s is start and e is end, inclusive.
	if s > e {
		panic("safesort_Array: s > e")
	}

	safesort_Partition := func(lo, hi, pivot_idx int) int {
		/* this is Hoare's algorithm */

		s := lo
		e := hi

		for {
			for s <= e && cmp(s, pivot_idx) {
				s++
			}

			if s > hi {
				// all values were < pivot, including the pivot itself!

				if pivot_idx != hi {
					arr.SwapElems(pivot_idx, hi)
				}

				return hi - 1
			}

			for e >= s && !cmp(e, pivot_idx) {
				e--
			}

			if e < lo {
				// all values were >= pivot

				if pivot_idx != lo {
					arr.SwapElems(pivot_idx, lo)
				}

				return lo
			}

			if s < e {
				arr.SwapElems(s, e)

				if pivot_idx == s {
					pivot_idx = e
				} else if pivot_idx == e {
					pivot_idx = s
				}

				s++
				e--
				continue
			}

			return s - 1
		}
	}

	for s < e {
		// handle the two element case (trivially)
		if s == e-1 {
			if cmp(e, s) {
				arr.SwapElems(s, e)
			}
			break
		}

		// choosing a pivot in the middle should avoid the worst-case
		// behavior that otherwise occurs when pivot is first or last
		// element and the array is already sorted.
		pivot_idx := (s + e) >> 1

		mid := safesort_Partition(s, e, pivot_idx)

		// handle degenerate cases
		if mid <= s {
			s++
			continue
		}
		if mid+1 >= e {
			e--
			continue
		}

		// only use recursion on the smallest group
		// [ it helps to limit stack usage ]
		if (mid - s) < (e - mid) {
			safesort_Array(arr, s, mid, cmp)
			s = mid + 1
		} else {
			safesort_Array(arr, mid+1, e, cmp)
			e = mid
		}
	}
}

//----------------------------------------------------------------------

func BI_Add(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind == VAL_String && b.Kind == VAL_String {
		SetResultString(a.Str + b.Str)
		return
	}

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("add primitive requires two integers or two strings")
		return
	}

	SetResultInt(a.Int + b.Int)
}

func BI_Subtract(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("sub primitive requires integers")
		return
	}

	SetResultInt(a.Int - b.Int)
}

func BI_Multiply(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("mul primitive requires integers")
		return
	}

	SetResultInt(a.Int * b.Int)
}

func BI_Divide(data_p int) {
	num := data_stack[data_p+1]
	div := data_stack[data_p+2]

	if num.Kind != VAL_Int || div.Kind != VAL_Int {
		Die("div primitive requires integers")
		return
	}

	if div.Int == 0 {
		Die("division by zero")
		return
	}

	SetResultInt(num.Int / div.Int)
}

func BI_Modulo(data_p int) {
	num := data_stack[data_p+1]
	div := data_stack[data_p+2]

	if num.Kind != VAL_Int || div.Kind != VAL_Int {
		Die("mod primitive requires integers")
		return
	}

	if div.Int == 0 {
		Die("division by zero")
		return
	}

	SetResultInt(num.Int % div.Int)
}

func BI_Power(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("pow primitive requires integers")
		return
	}

	// handle some special cases...
	switch {
	case b.Int < 0:
		Die("pow primitive requires exponent >= 0")
		return

	case b.Int == 0:
		SetResultInt(1)
		return

	case b.Int == 1:
		SetResultInt(a.Int)
		return

	case a.Int == 0 || a.Int == 1 || a.Int == NIL:
		SetResultInt(a.Int)
		return

	case a.Int == -1:
		if (b.Int % 2) == 0 {
			SetResultInt(1)
		} else {
			SetResultInt(-1)
		}
	}

	// here we have abs(a) >= 2 && b >= 2.
	// hence any b >= 32 will definitely overflow an int32.

	if b.Int >= 32 {
		SetResultNIL()
		return
	}

	res := int64(a.Int)

	for i := b.Int; i > 1; i-- {
		res *= int64(a.Int)

		if res < -0x7fffffff || res > 0x7fffffff {
			// overflow
			SetResultNIL()
			return
		}
	}

	SetResultInt(int32(res))
}

func BI_Abs(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("abs primitive requires an integer")
		return
	}

	// NIL is also the lowest negative number, and its absolute
	// value does not fit into an int32, so keeping NIL as NIL
	// sorta kinda makes sense.

	if v.Int == NIL {
		// no change
	} else if v.Int < 0 {
		SetResultInt(-v.Int)
	} else {
		SetResultInt(v.Int)
	}
}

func BI_Min(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	cmp := BI_raw_compare(a, b)

	if run_error != nil {
		return
	}

	if cmp <= 0 {
		SetResult(a)
	} else {
		SetResult(b)
	}
}

func BI_Max(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	cmp := BI_raw_compare(a, b)

	if run_error != nil {
		return
	}

	if cmp >= 0 {
		SetResult(a)
	} else {
		SetResult(b)
	}
}

//----------------------------------------------------------------------

func BI_BitAnd(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("bit-and primitive requires integers")
		return
	}

	SetResultInt(a.Int & b.Int)
}

func BI_BitOr(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("bit-or primitive requires integers")
		return
	}

	SetResultInt(a.Int | b.Int)
}

func BI_BitXor(data_p int) {
	a := data_stack[data_p+1]
	b := data_stack[data_p+2]

	if a.Kind != VAL_Int || b.Kind != VAL_Int {
		Die("bit-xor primitive requires integers")
		return
	}

	SetResultInt(a.Int ^ b.Int)
}

func BI_BitNot(data_p int) {
	v := data_stack[data_p+1]

	if v.Kind != VAL_Int {
		Die("bit-not primitive requires integers")
		return
	}

	SetResultInt(^v.Int)
}

func BI_ShiftLeft(data_p int) {
	num := data_stack[data_p+1]
	count := data_stack[data_p+2]

	if num.Kind != VAL_Int || count.Kind != VAL_Int {
		Die("shift-left requires integers")
		return
	}

	res := num.Int

	if count.Int > 0 {
		res = res << uint(count.Int)
	}
	if count.Int < 0 {
		res = res >> uint(-count.Int)
	}

	SetResultInt(res)
}

func BI_ShiftRight(data_p int) {
	num := data_stack[data_p+1]
	count := data_stack[data_p+2]

	if num.Kind != VAL_Int || count.Kind != VAL_Int {
		Die("shift-right requires integers")
		return
	}

	res := num.Int

	if count.Int > 0 {
		res = res >> uint(count.Int)
	}
	if count.Int < 0 {
		res = res << uint(-count.Int)
	}

	SetResultInt(res)
}

//----------------------------------------------------------------------

func BI_TypeInt(data_p int) {
	v := data_stack[data_p+1]

	SetResultBool(v.Kind == VAL_Int)
}

func BI_TypeString(data_p int) {
	v := data_stack[data_p+1]

	SetResultBool(v.Kind == VAL_String)
}

func BI_TypeArray(data_p int) {
	v := data_stack[data_p+1]

	SetResultBool(v.Kind == VAL_Array)
}

func BI_TypeMap(data_p int) {
	v := data_stack[data_p+1]

	SetResultBool(v.Kind == VAL_Map)
}

func BI_TypeFunction(data_p int) {
	v := data_stack[data_p+1]

	SetResultBool(v.Kind == VAL_Function)
}

//----------------------------------------------------------------------

func BI_RandSeed(data_p int) {
	seed := data_stack[data_p+1]

	if seed.Kind != VAL_Int {
		Die("rand-seed requires an integer")
		return
	}

	rand.Seed(int64(seed.Int))

	SetResultNIL()
}

func BI_RandInt(data_p int) {
	mod := data_stack[data_p+1]

	if mod.Kind != VAL_Int || mod.Int <= 0 {
		Die("rand-int requires a positive integer")
		return
	}

	// result will satisfy: 0 <= x < mod

	res := rand.Int31n(mod.Int)

	SetResultInt(res)
}

func BI_RandRange(data_p int) {
	low := data_stack[data_p+1]
	high := data_stack[data_p+2]

	if low.Kind != VAL_Int || high.Kind != VAL_Int {
		Die("rand-range requires integers")
		return
	}

	// result will satisfy: low <= x <= high

	if high.Int < low.Int {
		Die("rand-range requires low <= high")
		return
	}

	if high.Int == low.Int {
		SetResultInt(low.Int)
		return
	}

	val := rand.Int63n(int64(high.Int) - int64(low.Int) + 1)
	val += int64(low.Int)

	SetResultInt(int32(val))
}

//----------------------------------------------------------------------

func BI_Error(data_p int) {
	msg := data_stack[data_p+1]

	if msg.Kind != VAL_String {
		Die("error message must be a string")
		return
	}

	Die("program error: %s", msg.Str)
}

func BI_Print(data_p int) {
	msg := data_stack[data_p+1]

	if msg.Kind != VAL_String {
		Die("print message must be a string")
		return
	}

	fmt.Printf("%s\n", msg.Str)
	did_print = true

	SetResultNIL()
}

func BI_Input(data_p int) {
	prompt := data_stack[data_p+1]

	if prompt.Kind != VAL_String {
		Die("input prompt must be a string")
		return
	}

	var s string
	var err error

	if editor != nil {
		// use the line editor
		s, err = editor.Prompt(prompt.Str)

	} else {
		// read directly from Stdin
		var sb strings.Builder

		fmt.Printf("%s", prompt.Str)

		// with C, we would need a fflush(stdout) here, but with Go
		// I think it is unnecessary (since there is no flush-like
		// method on File to call).

		for {
			var buf [1]byte
			var n int

			n, err = os.Stdin.Read(buf[:])

			if n == 0 {
				if err == nil {
					// this should never happen
					err = fmt.Errorf("bad read on Stdin")
					break
				}

				// allow unterminated line before EOF
				if err == io.EOF && sb.Len() > 0 {
					err = nil
				}

				break
			}

			if buf[0] == '\r' || buf[0] == 0 {
				// skip CRs and NULs
				continue
			}

			if buf[0] == '\n' {
				// we have reached end-of-line
				err = nil
				break
			}

			sb.Write(buf[:])
		}

		s = sb.String()
	}

	if err == io.EOF {
		// NOTE: following does not makes sense if Stdin is a pipe
		//// fmt.Fprintf(os.Stderr, "\nEOF\n")
		SetResult(*globals[GLOB_EOF].loc)
		return
	}

	if err == liner.ErrPromptAborted {
		Die("aborted\n")
		return
	}

	// assume other errors are unrecoverable
	if err != nil {
		Die("%s", err.Error())
		return
	}

	// remember history (line editor only)
	if editor != nil {
		editor.AppendHistory(s)
	}

	SetResultString(s)
}

func BI_EnableEditor(data_p int) {
	EnableEditor()

	SetResultNIL()
}
