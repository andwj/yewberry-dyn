// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"

type Closure struct {
	// when this is non-nil, this is a built-in function, and ALL the
	// other fields in this struct are meaningless.
	builtin *Builtin

	// the uncompiled lambda expression, starting with the parameters.
	// guaranteed to be non-empty.
	uncompiled []*Token

	// the compiled form is a sequence of operations
	vm_ops []Operation

	// the parameter names of the function
	parameters []string

	// all the constant values used in the function
	constants []Value

	// locals created in this closure (such as parameters and let bindings).
	// the value is the data stack offset, e.g. first parameter is +1.
	locals *LocalGroup

	// the parent function which contains this one.  Will be nil for
	// global functions, and non-nil for anonymous lambda functions.
	parent *Closure

	// the variables which need to be captured when this *template*
	// closure is instantiated into a function value.  it may include
	// variables which are not directly used in a lambda but *are*
	// used in a sub-lambda (or sub-sub-lambda, etc).
	// using pointers since we share the structs (original is in the
	// LocalGroup struct).
	captures []*LocalVar

	// the variables which *were* captured in this function value
	// when the template closure was instantiated.
	// OP_UPV_READ/WRITE operations access this array.
	// using pointers since these are shared (e.g. FuncCallState).
	upvalues []*Upvalue

	// this is used while compiling, and is the highest data stack
	// offset used so far.  Starts off at zero, as +0 is reserved
	// for the function's return value.  It decreases when a let
	// scope (etc) has finished.
	high_water int16

	debug_name string

	cur_line int
}

type LocalGroup struct {
	vars map[string]*LocalVar

	parent *LocalGroup
}

type LocalVar struct {
	name      string
	offset    int16
	captured  bool
	immutable bool
	owner     *Closure
}

type Upvalue struct {
	// local var which got captured
	lvar *LocalVar

	// stack offset if alive, or -1 if hoisted
	offset int

	// pointer to stack slot if alive, 'hoisted' field otherwise
	loc *Value

	// storage for a hoisted variable
	hoisted Value
}

func CompileFunction(cl *Closure, debug_name string) error {
	cl.vm_ops = make([]Operation, 0)
	cl.captures = make([]*LocalVar, 0)
	cl.upvalues = make([]*Upvalue, 0)
	cl.debug_name = debug_name

	cl.PushLocalGroup()

	// get the parameter names...
	pars := cl.uncompiled[0]

	cl.cur_line = pars.LineNum

	if pars.Kind != TOK_Expr {
		return ParsingError(pars, "bad parameter list, names must be in ()")
	}

	cl.parameters = make([]string, len(pars.Children))

	for i, tok := range pars.Children {
		if tok.Kind != TOK_Name {
			return ParsingError(tok, "bad parameter name: %s", tok.String())
		}

		if cl.locals.vars[tok.Str] != nil {
			return ParsingError(tok, "duplicate parameter name '%s'", tok.Str)
		}

		cl.parameters[i] = tok.Str
		cl.AddLocal(tok.Str, false)
	}

	// compile the function body...
	if len(cl.uncompiled) < 2 {
		return ParsingError(pars, "missing body in fun")
	}

	body := cl.uncompiled[1:]

	err := cl.CompileBlock(body, pars, true /* tail */)
	if err != nil {
		return err
	}

	cl.PopLocalGroup()

	cl.Emit3(OP_RETURN, NO_PART, NO_PART)

	dump_var := global_lookup["DUMP-CODE"]
	if globals[dump_var].loc.IsTrue() {
		DumpOps(cl)
	}

	return Ok
}

func CompileRawExpr(t *Token) (*Closure, error) {
	if t.Kind != TOK_Expr {
		panic("CompileRawExpr: bad token")
	}

	cl := new(Closure)
	cl.vm_ops = make([]Operation, 0)
	cl.captures = make([]*LocalVar, 0)
	cl.upvalues = make([]*Upvalue, 0)
	cl.debug_name = fmt.Sprintf("#Expr#")

	cl.PushLocalGroup()

	cl.cur_line = t.LineNum

	err := cl.CompileExpr(t, false /* tail */)
	if err != nil {
		return nil, err
	}

	cl.PopLocalGroup()

	cl.Emit3(OP_RETURN, NO_PART, NO_PART)

	dump_var := global_lookup["DUMP-CODE"]
	if globals[dump_var].loc.IsTrue() {
		DumpOps(cl)
	}

	return cl, Ok
}

func (cl *Closure) CompileBlock(children []*Token, lnum *Token, tail bool) error {
	if len(children) == 0 {
		return ParsingError(lnum, "missing statement (block is empty)")
	}

	save_hw := cl.high_water

	cl.PushLocalGroup()

	// compile each child expression.
	// the last one will provide the final "result" without having
	// to do anything special.

	for idx, sub := range children {
		is_last := (idx == len(children)-1)

		// the "var" form is restricted to use inside a block,
		// so we must handle it here....

		if sub.Kind == TOK_Expr &&
			len(sub.Children) > 0 &&
			sub.Children[0].Match("var") {

			cl.cur_line = sub.LineNum

			err := cl.CompileVar(sub)
			if err != nil {
				return err
			}

			continue
		}

		err := cl.CompileToken(sub, tail && is_last)
		if err != nil {
			return err
		}
	}

	// this also adds code to hoist captured vars
	cl.PopLocalGroup()

	// restore the high water mark
	cl.high_water = save_hw

	return Ok
}

func (cl *Closure) CompileToken(t *Token, tail bool) error {
	cl.cur_line = t.LineNum

	switch t.Kind {
	case TOK_Expr:
		return cl.CompileExpr(t, tail)

	case TOK_Array:
		return cl.CompileArray(t)

	case TOK_Map:
		return cl.CompileMap(t)

	case TOK_Name:
		return cl.CompileName(t)

	default:
		return cl.CompileImmediate(t)
	}
}

func (cl *Closure) Emit4(T OpCode, S, M, D int16) {
	oper := Operation{T, S, M, D, cl.cur_line}
	cl.vm_ops = append(cl.vm_ops, oper)
}

func (cl *Closure) Emit3(T OpCode, S, D int16) {
	cl.Emit4(T, S, NO_PART, D)
}

func (cl *Closure) EmitCopy0(D int16) {
	// this method emits OP_COPY with S=0 and the given D, *unless*
	// the previous operation is an OP_COPY or OP_GLOB_READ which
	// has D=0 -- in that case we just modify that operation.
	// [ this is a peephole optimization ]

	if len(cl.vm_ops) > 0 {
		last := &cl.vm_ops[len(cl.vm_ops)-1]
		if last.D == 0 && (last.T == OP_COPY || last.T == OP_GLOB_READ) {
			last.D = D
			return
		}
	}

	cl.Emit4(OP_COPY, 0, NO_PART, D)
}

func (cl *Closure) AddConstant(v Value) int16 {
	if cl.constants == nil {
		cl.constants = make([]Value, 0)
	}

	// see if we can re-use an existing constant
	for idx, other := range cl.constants {
		if v.BasicEqual(&other) {
			return int16(-1 - idx)
		}
	}

	// nope, so add a new one
	res := int16(-1 - len(cl.constants))

	cl.constants = append(cl.constants, v)

	return res
}

func (cl *Closure) PushLocalGroup() {
	group := new(LocalGroup)
	group.vars = make(map[string]*LocalVar)
	group.parent = cl.locals

	cl.locals = group
}

func (cl *Closure) PopLocalGroup() {
	if cl.locals == nil {
		panic("PopLocalGroup with empty locals")
	}

	// add code to hoist any captured vars
	for _, lvar := range cl.locals.vars {
		if lvar.captured {
			cl.Emit3(OP_HOIST_VAR, lvar.offset, NO_PART)
		}
	}

	cl.locals = cl.locals.parent
}

func (cl *Closure) AddLocal(name string, immutable bool) int16 {
	if cl.locals == nil {
		panic("AddLocal when closure has no LocalGroup")
	}

	cl.high_water += 1
	offset := cl.high_water

	lvar := new(LocalVar)
	lvar.name = name
	lvar.offset = offset
	lvar.immutable = immutable
	lvar.owner = cl

	cl.locals.vars[name] = lvar
	return offset
}

//----------------------------------------------------------------------

func (cl *Closure) CompileName(t *Token) error {
	name := t.Str

	// find scope of the name
	lvar := cl.LookupLocal(name)

	if lvar == nil {
		// identifier is not a local, try a global
		g_idx, exist := global_lookup[name]
		if !exist {
			return ParsingError(t, "unknown binding: '%s'", name)
		}
		cl.Emit3(OP_GLOB_READ, int16(g_idx), 0)

	} else if lvar.owner == cl {
		// local identifiers exist in current stack frame
		cl.Emit3(OP_COPY, lvar.offset, 0)

	} else {
		// variable is local to a parent -- an upvalue
		upv_idx := cl.CaptureLocal(lvar)
		cl.Emit3(OP_UPV_READ, int16(upv_idx), 0)
	}

	return Ok
}

func (cl *Closure) LookupLocal(name string) *LocalVar {
	for p := cl; p != nil; p = p.parent {
		for group := p.locals; group != nil; group = group.parent {
			lvar := group.vars[name]

			if lvar != nil {
				// found it.
				return lvar
			}
		}
	}

	// not found
	return nil
}

func (cl *Closure) CaptureLocal(lvar *LocalVar) int {
	lvar.captured = true

	idx := cl.AddCapture(lvar)

	// if this closure is a sub-lambda inside a lambda
	// (or a sub-sub-lambda, etc) then we rely on all lambdas
	// above us to perform a capture, since the variable may
	// cease to exist on the stacks by the time this closure
	// gets instantiated.

	// since the owner *must* be higher up the chain, we know
	// cl.parent != nil and 'p' *will* eventually reach the
	// owner in this loop.

	for p := cl.parent; p != lvar.owner; p = p.parent {
		p.AddCapture(lvar)
	}

	return idx
}

func (cl *Closure) AddCapture(lvar *LocalVar) int {
	// check if already in the list...
	for idx, ptr := range cl.captures {
		if ptr == lvar {
			return idx
		}
	}

	idx := len(cl.captures)
	cl.captures = append(cl.captures, lvar)

	return idx
}

func (cl *Closure) CompileImmediate(t *Token) error {
	// this handles simple values: integers and strings
	var lit Value

	err := lit.ParseLiteral(t)
	if err != nil {
		return err
	}

	cl.Emit3(OP_COPY, cl.AddConstant(lit), 0)
	return Ok
}

func (cl *Closure) CompileArray(t *Token) error {
	cl.high_water += 1
	arr_ofs := cl.high_water

	// create a new empty array
	cl.Emit3(OP_MAKE_ARRAY, NO_PART, arr_ofs)

	// append each element in turn.
	for _, sub := range t.Children {
		err := cl.CompileToken(sub, false)
		if err != nil {
			return err
		}

		cl.Emit3(OP_APPEND, 0, arr_ofs)
	}

	cl.Emit3(OP_COPY, arr_ofs, 0)

	cl.high_water -= 1

	return Ok
}

func (cl *Closure) CompileMap(t *Token) error {
	if (len(t.Children) % 2) != 0 {
		return ParsingError(t, "bad map literal: odd number of elements")
	}

	cl.high_water += 1
	map_ofs := cl.high_water

	cl.high_water += 1
	key_ofs := cl.high_water

	// create a new empty map
	cl.Emit3(OP_MAKE_MAP, NO_PART, map_ofs)

	// insert each element in turn.
	for i := 0; i < len(t.Children); i += 2 {
		key := t.Children[i]
		val := t.Children[i+1]

		err := cl.CompileToken(key, false)
		if err != nil {
			return err
		}
		cl.EmitCopy0(key_ofs)

		err = cl.CompileToken(val, false)
		if err != nil {
			return err
		}

		cl.Emit4(OP_ELEM_WRITE, 0 /* val */, key_ofs, map_ofs)
	}

	cl.Emit3(OP_COPY, map_ofs, 0)

	cl.high_water -= 2

	return Ok
}

//----------------------------------------------------------------------

func (cl *Closure) CompileExpr(t *Token, tail bool) error {
	if len(t.Children) == 0 {
		return ParsingError(t, "empty expression in ()")
	}

	head := t.Children[0]

	// check for special forms...
	switch {
	case head.Match("$"):
		return cl.CompileConcat(t)

	case head.Match("begin"):
		return cl.CompileBegin(t, tail)

	case head.Match("lam"):
		return cl.CompileLambda(t)

	case head.Match("set!"):
		return cl.CompileAssign(t)

	case head.Match("if"):
		return cl.CompileIf(t, tail)

	case head.Match("while"):
		return cl.CompileWhile(t)

	case head.Match("for"):
		return cl.CompileForRange(t)

	case head.Match("for-each"):
		return cl.CompileForEach(t)

	case head.Match("var"):
		return ParsingError(t, "cannot use 'var' in that context")

	case head.Match("fun"), head.Match("macro"):
		return ParsingError(t, "cannot use '%s' within code", head.Str)
	}

	// a math expression?
	if HasOperator(t) {
		return cl.CompileMathExpr(t)
	}

	// this is a standard func-call (or array/map access)
	return cl.CompileFuncCall(t, tail)
}

func (cl *Closure) CompileFuncCall(t *Token, tail bool) error {
	// hw+0 = function itself
	// hw+1 = base of stack frame (result, params)

	save_hw := cl.high_water

	cl.high_water += 1
	base := cl.high_water

	// first child is function itself, not a parameter
	num_params := len(t.Children) - 1
	cl.high_water += int16(num_params)

	// compute each parameter (and function itself)
	for idx, sub := range t.Children {
		err := cl.CompileToken(sub, false)
		if err != nil {
			return err
		}
		cl.EmitCopy0(base + int16(idx))
	}

	// the function and its parameters are on the data stack,
	// so now we can call it.

	cl.cur_line = t.LineNum

	if tail {
		cl.Emit4(OP_TAIL_CALL, base, int16(num_params), NO_PART)

		// nothing else needed

	} else {
		cl.Emit4(OP_FUN_CALL, base, int16(num_params), NO_PART)

		// grab the result
		cl.Emit3(OP_COPY, base, 0)
	}

	// restore high water mark
	cl.high_water = save_hw

	return Ok
}

type Node struct {
	// this is non-nil for a term
	term *Token

	// if not a term, this is non-nil for the operator
	op *OperatorInfo

	// arguments to operator (R is nil for unary ops)
	L *Node
	R *Node
}

func (cl *Closure) CompileMathExpr(t *Token) error {
	/* this is Dijkstra's shunting-yard algorithm */

	op_stack := make([]*OperatorInfo, 0)
	term_stack := make([]*Node, 0)

	shunt := func() error {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		// possible??
		if len(term_stack) < 2 {
			return ParsingError(t, "FAILURE AT THE SHUNTING YARD")
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]
		term_stack = term_stack[0 : len(term_stack)-2]

		node := new(Node)
		node.op = op
		node.L = L
		node.R = R

		term_stack = append(term_stack, node)
		return Ok
	}

	// saved unary ops for the next (unseen) term
	un_ops := make([]*OperatorInfo, 0)

	seen_term := false

	for _, tok := range t.Children {
		var op *OperatorInfo

		if tok.Kind == TOK_Name {
			op = operators[tok.Str]
		}

		// if not an operator, it must be a term
		if op == nil {
			if seen_term {
				return ParsingError(tok,
					"bad math expression, missing operator between terms")
			}
			seen_term = true

			term := new(Node)
			term.term = tok

			// apply any saved unary operators
			for len(un_ops) > 0 {
				op = un_ops[len(un_ops)-1]
				un_ops = un_ops[0 : len(un_ops)-1]

				node := new(Node)
				node.op = op
				node.L = term

				term = node
			}

			term_stack = append(term_stack, term)
			continue
		}

		if op.unary {
			if seen_term {
				return ParsingError(tok,
					"bad math expression, unexpected %s operator", tok.Str)
			}
			un_ops = append(un_ops, op)
			continue
		}

		if !seen_term || len(un_ops) > 0 {
			return ParsingError(tok,
				"bad math expression, unexpected %s operator", tok.Str)
		}

		// shunt existing operators if they have greater precedence
		for len(op_stack) > 0 {
			top := op_stack[len(op_stack)-1]

			if top.precedence > op.precedence ||
				(top.precedence == op.precedence && !top.right_assoc) {

				err := shunt()
				if err != nil {
					return err
				}
				continue
			}

			break
		}

		op_stack = append(op_stack, op)
		seen_term = false
	}

	if !seen_term || len(un_ops) > 0 {
		return ParsingError(t.Children[len(t.Children)-1],
			"bad math expression, missing term after last operator")
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		err := shunt()
		if err != nil {
			return err
		}
	}

	if len(term_stack) != 1 {
		return ParsingError(t, "PARSE MATH FAIlURE")
	}

	return cl.CompileMathNode(term_stack[0], t)
}

func (cl *Closure) CompileMathNode(nd *Node, lnum *Token) error {
	if nd.term != nil {
		return cl.CompileToken(nd.term, false)
	}

	num_params := 1
	if !nd.op.unary {
		num_params = 2
	}

	// for the REPL, check the global def has not been replaced
	if nd.op.actual.loc.Kind != VAL_Function {
		return ParsingError(lnum, "operator '%s' lost its global def", nd.op.name)
	}

	/* this is analogous to a function call (see CompileExpr code) */

	save_hw := cl.high_water

	cl.high_water += 1
	base := cl.high_water

	// alloc room for parameters
	cl.high_water += int16(num_params)

	// push the actual function
	fun_idx := cl.AddConstant(*nd.op.actual.loc)

	cl.Emit3(OP_COPY, fun_idx, base)

	// push the arguments
	for arg := 0; arg < 2; arg++ {
		child := nd.L
		if arg == 1 {
			child = nd.R
		}
		if child == nil {
			continue
		}

		err := cl.CompileMathNode(child, lnum)
		if err != nil {
			return err
		}

		cl.EmitCopy0(base + 1 + int16(arg))
	}

	cl.Emit4(OP_FUN_CALL, base, int16(num_params), NO_PART)

	// grab the result
	cl.Emit3(OP_COPY, base, 0)

	// restore high water mark
	cl.high_water = save_hw

	return Ok
}

func (cl *Closure) CompileAssign(t *Token) error {
	if len(t.Children) < 3 {
		return ParsingError(t, "bad assignment syntax")
	}

	targ := t.Children[1]
	nval := t.Children[len(t.Children)-1]
	indexes := t.Children[2 : len(t.Children)-1]

	// handle array/map access
	if len(indexes) > 0 {
		return cl.CompileSetArrMap(targ, nval, indexes)
	}

	if targ.Kind != TOK_Name {
		return ParsingError(t, "expected var name, got: %s", targ.String())
	}

	// compute the value
	err := cl.CompileToken(nval, false)
	if err != nil {
		return err
	}

	name := targ.Str

	// find scope of the name
	lvar := cl.LookupLocal(name)

	if lvar != nil && lvar.immutable {
		return ParsingError(t, "loop variable '%s' is read-only", name)
	}

	if lvar == nil {
		// identifier is not a local, try a global
		g_idx, exist := global_lookup[name]
		if !exist {
			return ParsingError(t, "unknown binding: '%s'", name)
		}
		cl.Emit3(OP_GLOB_WRITE, 0, int16(g_idx))

	} else if lvar.owner == cl {
		// var is local to this function
		cl.EmitCopy0(lvar.offset)

	} else {
		// var is local to a parent -- an upvalue
		upv_idx := cl.CaptureLocal(lvar)
		cl.Emit3(OP_UPV_WRITE, 0, int16(upv_idx))
	}

	// ensure the result is always NIL
	cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)

	return Ok
}

func (cl *Closure) CompileSetArrMap(targ, nval *Token, indexes []*Token) error {
	// we need two temporaries
	cl.high_water += 1
	arr_ofs := cl.high_water

	cl.high_water += 1
	key_ofs := cl.high_water

	// compute the array/map itself
	err := cl.CompileToken(targ, false)
	if err != nil {
		return err
	}
	cl.EmitCopy0(arr_ofs)

	for i := 0; i < len(indexes)-1; i++ {
		err := cl.CompileToken(indexes[i], false)
		if err != nil {
			return err
		}

		cl.EmitCopy0(key_ofs)

		cl.Emit4(OP_ELEM_READ, arr_ofs, key_ofs, arr_ofs)
	}

	// the final index or key
	err = cl.CompileToken(indexes[len(indexes)-1], false)
	if err != nil {
		return err
	}
	cl.EmitCopy0(key_ofs)

	// compute new value, write it into the object
	err = cl.CompileToken(nval, false)
	if err != nil {
		return err
	}
	cl.Emit4(OP_ELEM_WRITE, 0, key_ofs, arr_ofs)

	// the result of an assignment is always NIL
	cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)

	// restore the high water mark
	cl.high_water -= 2

	return Ok
}

func (cl *Closure) CompileBegin(t *Token, tail bool) error {
	children := t.Children[1:]

	// no elements?
	if len(children) == 0 {
		cl.Emit3(OP_GLOB_READ, int16(GLOB_NIL), 0)
		return Ok
	}

	return cl.CompileBlock(children, t, tail)
}

func (cl *Closure) CompileConcat(t *Token) error {
	children := t.Children[1:]

	// begin with an empty string
	cl.high_water += 1
	build_ofs := cl.high_water

	var empty_str Value
	empty_str.MakeString("")
	cl.Emit3(OP_COPY, cl.AddConstant(empty_str), build_ofs)

	var space_str Value
	space_str.MakeString(" ")
	space_ofs := cl.AddConstant(space_str)

	for idx, sub := range children {
		if idx > 0 {
			cl.Emit3(OP_APPEND, space_ofs, build_ofs)
		}

		err := cl.CompileToken(sub, false)
		if err != nil {
			return err
		}

		// OPTIMIZE: when sub is a string literal

		cl.Emit3(OP_TOSTR, 0, 0)
		cl.Emit3(OP_APPEND, 0, build_ofs)
	}

	cl.Emit3(OP_COPY, build_ofs, 0)

	cl.high_water -= 1

	return Ok
}

func (cl *Closure) CompileLambda(t *Token) error {
	if len(t.Children) < 2 {
		return ParsingError(t, "missing parameter list")
	}

	// compile code into a template closure
	template := new(Closure)
	template.uncompiled = t.Children[1:]
	template.parent = cl

	err := CompileFunction(template, cl.debug_name+".lam")
	if err != nil {
		return ParsingError(t, "%s", err.Error())
	}

	var v Value
	v.MakeFunction(template)

	// this operation creates the "real" closure when run
	cl.Emit3(OP_MAKE_FUNC, cl.AddConstant(v), 0)

	return Ok
}

func (cl *Closure) CompileVar(t *Token) error {
	children := t.Children[1:]

	if len(children) < 2 {
		return ParsingError(t, "missing var name or value")
	}
	if len(children) > 2 {
		return ParsingError(t, "bad variable def, too many values")
	}

	// create a local variable binding
	t_var := children[0]
	t_exp := children[1]

	if t_var.Kind != TOK_Name {
		return ParsingError(t, "expected var name, got: %s", t_var.String())
	}
	if !ValidDefName(t_var.Str) {
		return ParsingError(t, "bad var name: '%s' is a reserved word", t_var.Str)
	}

	// compile code to generate the value
	err := cl.CompileToken(t_exp, false)
	if err != nil {
		return err
	}

	// allocate slot for local var
	if cl.locals.vars[t_var.Str] != nil {
		return ParsingError(t, "duplicate var name '%s'", t_var.Str)
	}
	var_ofs := cl.AddLocal(t_var.Str, false /* immutable */)

	cl.EmitCopy0(var_ofs)

	// force result to be NIL
	cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)

	return Ok
}

func (cl *Closure) CompileIf(t *Token, tail bool) error {
	// divide the tokens into clauses, where each clause
	// begins with an "if", "elif" or "else" keyword.

	clauses := make([][]*Token, 0)
	clauses = append(clauses, t.Children[0:1])

	cur_start := 0

	for i := 1; i < len(t.Children); i++ {
		tok := t.Children[i]

		if tok.Match("elif") || tok.Match("else") {
			// start a new clause
			cur_start = i
			clauses = append(clauses, t.Children[i:i+1])

		} else {
			// extend the current clause
			L := len(clauses) - 1
			clauses[L] = t.Children[cur_start : i+1]
		}
	}

	/* DEBUG
	fmt.Printf("NUM CLAUSES: %d\n", len(clauses))
	for i, cl := range clauses {
		fmt.Printf("   %d: len %d keyword %q\n", i, len(cl), cl[0].Str)
	} */

	// operations we need to fix for the end position
	end_jump_pcs := make([]int, 0)

	if_false_pc := -1

	has_else := false

	for idx, grp := range clauses {
		// check stuff
		is_last := (idx == len(clauses)-1)
		is_else := (grp[0].Str == "else")

		if is_else && !is_last {
			return ParsingError(grp[0], "else clause must be last in if")
		}

		if len(grp) < 2 || (!is_else && len(grp) < 3) {
			return ParsingError(grp[0], "%s clause is too short", grp[0].Str)
		}

		if if_false_pc > 0 {
			// fixup the previous OP_IF_FALSE
			next_pc := len(cl.vm_ops)
			cl.vm_ops[if_false_pc].D = int16(next_pc)
		}

		if is_else {
			err := cl.CompileBlock(grp[1:], grp[0], tail)
			if err != nil {
				return err
			}

			has_else = true
			continue
		}

		// compile the condition
		err := cl.CompileToken(grp[1], false)
		if err != nil {
			return err
		}

		if_false_pc = len(cl.vm_ops)
		cl.Emit3(OP_IF_FALSE, 0, -7 /* dummy value */)

		// compile the block
		err = cl.CompileBlock(grp[2:], grp[0], tail)
		if err != nil {
			return err
		}

		// jump to the very end
		pc := len(cl.vm_ops)
		cl.Emit3(OP_JUMP, NO_PART, -8 /* dummy value */)

		end_jump_pcs = append(end_jump_pcs, pc)
	}

	// else clause is optional, handle the absent case
	if !has_else {
		// fixup the last OP_IF_FALSE
		next_pc := len(cl.vm_ops)
		cl.vm_ops[if_false_pc].D = int16(next_pc)

		// default is like "else NIL"
		cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)
	}

	// fix the end targets
	end_pc := len(cl.vm_ops)

	for _, pc := range end_jump_pcs {
		cl.vm_ops[pc].D = int16(end_pc)
	}

	// target of each jump to the end.
	// this OP_NOP inhibits any erroneous optimization by EmitCopy0().
	cl.Emit3(OP_NOP, NO_PART, NO_PART)

	return Ok
}

func (cl *Closure) CompileWhile(t *Token) error {
	children := t.Children[1:]

	if len(children) < 2 {
		return ParsingError(t, "wrong # args to while")
	}

	start_pc := len(cl.vm_ops)

	// compile condition expr
	cond := children[0]

	err := cl.CompileToken(cond, false)
	if err != nil {
		return err
	}

	// jump to end if value is not true
	if_pc := len(cl.vm_ops)
	cl.Emit3(OP_IF_FALSE, 0, -6 /* dummy value */)

	// compile the body
	body := children[1:]

	err = cl.CompileBlock(body, t, false)
	if err != nil {
		return err
	}

	// jump back to beginning
	cl.Emit3(OP_JUMP, NO_PART, int16(start_pc))

	end_pc := len(cl.vm_ops)

	cl.vm_ops[if_pc].D = int16(end_pc)

	// ensure the result is always NIL
	// [ this is also target of IF_FALSE jump ]
	cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)

	return Ok
}

func (cl *Closure) CompileForRange(t *Token) error {
	children := t.Children[1:]

	if len(children) < 4 {
		return ParsingError(t, "wrong # args to for")
	}

	save_hw := cl.high_water
	cl.PushLocalGroup()

	// get the variable name
	t_var := children[0]

	if t_var.Kind != TOK_Name {
		return ParsingError(t, "expected var name, got: %s", t_var.String())
	}
	if !ValidDefName(t_var.Str) {
		return ParsingError(t, "bad var name: '%s' is a reserved word", t_var.Str)
	}

	// need three stack slots: the var, the target and the step.
	// they MUST be contiguous -- the helper ops assume this.
	var_ofs := cl.AddLocal(t_var.Str, true)

	cl.high_water += 1
	targ_ofs := cl.high_water
	_ = targ_ofs

	cl.high_water += 1
	step_ofs := cl.high_water
	_ = step_ofs

	// compute the start and end expressions
	for i := 1; i <= 2; i++ {
		err := cl.CompileToken(children[i], false)
		if err != nil {
			return err
		}

		cl.EmitCopy0(var_ofs + int16(i) - 1)
	}

	// compute the step
	cl.Emit3(OP_FOR_BEGIN, var_ofs, NO_PART)

	// run the body -- it will always run at least once
	// [ unless we extend the syntax with an explicit step... ]
	body := children[3:]
	body_pc := len(cl.vm_ops)

	err := cl.CompileBlock(body, t, false)
	if err != nil {
		return err
	}

	// add the step, loop back if var has not gone past target
	cl.Emit3(OP_FOR_LOOP, var_ofs, int16(body_pc))

	// ensure the result is always NIL
	cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)

	// restore stuff
	cl.PopLocalGroup()
	cl.high_water = save_hw

	return Ok
}

func (cl *Closure) CompileForEach(t *Token) error {
	children := t.Children[1:]

	if len(children) < 4 {
		return ParsingError(t, "wrong # args to for-each")
	}

	save_hw := cl.high_water
	cl.PushLocalGroup()

	// get the variable names
	t_idx := children[0]
	t_var := children[1]

	if t_idx.Kind != TOK_Name {
		return ParsingError(t, "expected var name, got: %s", t_idx.String())
	}
	if t_var.Kind != TOK_Name {
		return ParsingError(t, "expected var name, got: %s", t_var.String())
	}
	if !ValidDefName(t_idx.Str) {
		return ParsingError(t, "bad var name: '%s' is a reserved word", t_idx.Str)
	}
	if !ValidDefName(t_var.Str) {
		return ParsingError(t, "bad var name: '%s' is a reserved word", t_var.Str)
	}
	if t_idx.Str == t_var.Str {
		return ParsingError(t, "duplicate var name '%s'", t_idx.Str)
	}

	// need four stack slots: the two vars, the object, and iter state.
	// they MUST be contiguous -- the helper ops assume this.
	key_ofs := cl.AddLocal(t_idx.Str, true)
	var_ofs := cl.AddLocal(t_var.Str, true)

	cl.high_water += 1
	arr_ofs := cl.high_water

	cl.high_water += 1
	work_ofs := cl.high_water

	_ = var_ofs
	_ = work_ofs

	// compile the expression (the array/map/string)
	expr := children[2]

	err := cl.CompileToken(expr, false)
	if err != nil {
		return err
	}
	cl.EmitCopy0(arr_ofs)

	// begin the iteration: set the vars or jump to end
	begin_pc := len(cl.vm_ops)
	cl.Emit3(OP_EACH_BEGIN, key_ofs, -12 /* dummy dest */)

	// compile the body
	body := children[3:]
	body_pc := len(cl.vm_ops)

	err = cl.CompileBlock(body, t, false)
	if err != nil {
		return err
	}

	// loop the iteration: set next vars and jump, or stop
	cl.Emit3(OP_EACH_LOOP, key_ofs, int16(body_pc))

	// fix the dummy dest
	end_pc := len(cl.vm_ops)

	cl.vm_ops[begin_pc].D = int16(end_pc)

	// ensure the result is always NIL
	cl.Emit3(OP_GLOB_READ, GLOB_NIL, 0)

	// restore stuff
	cl.PopLocalGroup()
	cl.high_water = save_hw

	return Ok
}

func ParsingError(t *Token, format string, a ...interface{}) error {
	return LocErrorf(t.LineNum, format, a...)
}
