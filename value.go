// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "strings"
import "sort"

const NIL = int32(-0x80000000)
const TRUE = int32(0x7fffffff)

type ValueKind int

const (
	VAL_Int ValueKind = iota
	VAL_String
	VAL_Array
	VAL_Map
	VAL_Function
)

type Value struct {
	Kind ValueKind

	Int   int32
	Str   string
	Array *ArrayImpl
	Map   *MapImpl
	Clos  *Closure
}

type ArrayImpl struct {
	data []Value
}

type MapImpl struct {
	// we are forced to use a pointer to Value here, because Go
	// does not allow taking the address of elements in a map.
	data map[string]*Value
}

// BindingGroup contains the memory locations where variables are
// stored.  Once created, variables never change their address,
// although variables may cease to exist (e.g. when a let scope
// finishes) and then their memory can be reclaimed and re-used.
type BindingGroup struct {
	// this can stay nil until a binding is added
	locs map[string]*Value
}

//----------------------------------------------------------------------

func (kind ValueKind) String() string {
	switch kind {
	case VAL_Int:
		return "<Int>"
	case VAL_String:
		return "<String>"
	case VAL_Array:
		return "<Array>"
	case VAL_Map:
		return "<Map>"
	case VAL_Function:
		return "<Function>"
	default:
		return "!!!INVALID!!!"
	}
}

func (v *Value) ShallowCopy() Value {
	switch v.Kind {
	case VAL_Array:
		res := Value{}
		res.MakeArray(len(v.Array.data))
		copy(res.Array.data, v.Array.data)
		return res

	case VAL_Map:
		res := Value{}
		res.MakeMap()
		for key, ptr := range v.Map.data {
			res.KeySet(key, *ptr)
		}
		return res

	default:
		return *v
	}
}

func (v *Value) MakeInt(num int32) {
	v.Kind = VAL_Int
	v.Int = num
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeNIL() {
	v.Kind = VAL_Int
	v.Int = NIL
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeBool(b bool) {
	v.Kind = VAL_Int
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil

	if b {
		v.Int = TRUE
	} else {
		v.Int = NIL
	}
}

func (v *Value) MakeString(s string) {
	v.Kind = VAL_String
	v.Str = s
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeArray(size int) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, size)

	v.Kind = VAL_Array
	v.Array = impl
	v.Str = ""
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeMap() {
	impl := new(MapImpl)
	impl.data = make(map[string]*Value)

	v.Kind = VAL_Map
	v.Map = impl
	v.Str = ""
	v.Array = nil
	v.Clos = nil
}

func (v *Value) MakeFunction(cl *Closure) {
	v.Kind = VAL_Function
	v.Clos = cl
	v.Str = ""
	v.Array = nil
	v.Map = nil
}

//----------------------------------------------------------------------

func (v *Value) IsNIL() bool {
	return (v.Kind == VAL_Int) && (v.Int == NIL)
}

func (v *Value) IsTrue() bool {
	// everything except "NIL" is considered true
	if v.Kind == VAL_Int && v.Int == NIL {
		return false
	}
	return true
}

func (v *Value) BasicEqual(other *Value) bool {
	if v.Kind != other.Kind {
		return false
	}

	switch v.Kind {
	case VAL_Int:
		return v.Int == other.Int

	case VAL_String:
		return v.Str == other.Str

	case VAL_Function:
		return v.Clos == other.Clos

	default:
		// we do not compare arrays or maps
		return false
	}
}

func (v *Value) NumParameters() int {
	if v.Kind != VAL_Function {
		return 0
	}

	if v.Clos.builtin != nil {
		return v.Clos.builtin.args
	}

	return len(v.Clos.parameters)
}

//----------------------------------------------------------------------

func (v *Value) GetElem(idx int) Value {
	return v.Array.data[idx]
}

func (v *Value) SetElem(idx int, elem Value) {
	v.Array.data[idx] = elem
}

func (v *Value) SwapElems(i, k int) {
	A := v.Array.data[i]
	B := v.Array.data[k]

	v.Array.data[i] = B
	v.Array.data[k] = A
}

func (v *Value) AppendElem(elem Value) {
	v.Array.data = append(v.Array.data, elem)
}

func (v *Value) InsertElem(idx int, elem Value) {
	// an index <= zero will insert at the beginning (prepend).
	// an index >= length will insert at the end (append).

	if idx < 0 {
		idx = 0
	}
	if idx >= len(v.Array.data) {
		v.AppendElem(elem)
		return
	}

	// resize the array
	v.Array.data = append(v.Array.data, Value{})

	// shift elements up
	copy(v.Array.data[idx+1:], v.Array.data[idx:])

	// store new element
	v.Array.data[idx] = elem
}

func (v *Value) DeleteElem(idx int) {
	size := len(v.Array.data)

	// trying to remove non-existent elements is a no-op
	if idx < 0 || idx >= size {
		return
	}

	// shift elements down, if necessary
	if idx < size {
		copy(v.Array.data[idx:], v.Array.data[idx+1:])
	}

	// shrink the slice
	v.Array.data = v.Array.data[0 : size-1]
}

func (v *Value) ValidIndex(idx int) bool {
	if idx < 0 {
		return false
	}
	if idx >= len(v.Array.data) {
		return false
	}
	return true
}

func (v *Value) Length() int {
	switch v.Kind {
	case VAL_Array:
		return len(v.Array.data)

	case VAL_Map:
		return len(v.Map.data)

	case VAL_String:
		return len([]rune(v.Str))

	default:
		return 1
	}
}

//----------------------------------------------------------------------

func (v *Value) KeyExists(s string) bool {
	_, ok := v.Map.data[s]
	return ok
}

func (v *Value) KeyGet(s string) Value {
	ptr, ok := v.Map.data[s]
	if ok {
		return *ptr
	}

	res := Value{}
	res.MakeNIL()

	return res
}

func (v *Value) KeySet(s string, newval Value) {
	ptr, ok := v.Map.data[s]

	if !ok {
		ptr = new(Value)
		v.Map.data[s] = ptr
	}

	*ptr = newval
}

func (v *Value) KeyDelete(s string) {
	delete(v.Map.data, s)
}

func (v *Value) CollectKeys() Value {
	var list Value
	list.MakeArray(0)

	for key, _ := range v.Map.data {
		var v Value
		v.MakeString(key)
		list.AppendElem(v)
	}

	return list
}

//----------------------------------------------------------------------

func (bg *BindingGroup) Exists(name string) bool {
	_, ok := bg.locs[name]
	return ok
}

func (bg *BindingGroup) SetLoc(name string, loc *Value) {
	if bg.locs == nil {
		bg.locs = make(map[string]*Value)
	}

	bg.locs[name] = loc
}

func (bg *BindingGroup) GetLoc(name string) *Value {
	loc, exist := bg.locs[name]
	if exist {
		return loc
	}

	// create a new memory location for the variable
	loc = new(Value)
	loc.MakeNIL()

	bg.SetLoc(name, loc)

	return loc
}

func MakeGlobal(name string) *GlobalDef {
	idx, exist := global_lookup[name]
	if exist {
		return globals[idx]
	}

	// create a new memory location for the variable
	loc := new(Value)
	loc.MakeNIL()

	def := new(GlobalDef)
	def.loc = loc
	def.name = name

	global_lookup[name] = len(globals)
	globals = append(globals, def)

	return def
}

func DeleteGlobal(name string) {
	// this is only for the REPL, used to prevent a dud function
	// hanging around if compiling it failed.

	idx, exist := global_lookup[name]
	if exist {
		globals[idx].loc = new(Value)
		globals[idx].loc.MakeNIL()
	}

	delete(global_lookup, name)
}

//----------------------------------------------------------------------

func (v *Value) ParseLiteral(lit *Token) (err error) {
	switch lit.Kind {
	case TOK_Int:
		err = v.ParseInt(lit.Str)

	case TOK_Float:
		err = v.ParseFloat(lit.Str)

	case TOK_Char:
		err = v.ParseChar(lit.Str)

	case TOK_String:
		err = v.ParseString(lit.Str)

	case TOK_Expr:
		err = v.ParseExpr(lit.Children)

	case TOK_Array:
		err = v.ParseArray(lit.Children)

	case TOK_Map:
		err = v.ParseMap(lit.Children)

	case TOK_Name:
		err = v.ParseName(lit.Str)

	default:
		err = fmt.Errorf("bad literal value, got: %s", lit.String())
	}

	// ensure error line number is useful with big data structures
	if err != nil {
		err = LocErrorAt(err, lit.LineNum)
	}

	return
}

func (v *Value) ParseInt(s string) error {
	// we want to handle values >= 0x80000000, which would normally
	// overflow an int32.  so we use int64 and check for overflow.

	var long_val int64

	n, _ := fmt.Sscanf(s, "%v", &long_val)
	if n != 1 {
		return fmt.Errorf("bad integer '%s'", s)
	}

	if long_val < -0x80000000 || long_val >= 0xffffffff {
		return fmt.Errorf("integer too large for 32 bits")
	}

	if long_val >= 0x80000000 {
		long_val -= 0x100000000
	}

	v.MakeInt(int32(long_val))

	return Ok
}

func (v *Value) ParseFloat(s string) error {
	return fmt.Errorf("floating point numbers are not supported")
}

func (v *Value) ParseChar(s string) error {
	runes := []rune(s)

	if len(runes) == 0 {
		return fmt.Errorf("malformed character literal")
	}

	v.MakeInt(int32(runes[0]))

	return Ok
}

func (v *Value) ParseString(s string) error {
	// too easy!
	v.MakeString(s)

	return Ok
}

func (v *Value) ParseName(s string) error {
	// look up existing definition
	idx, exist := global_lookup[s]
	if !exist {
		return fmt.Errorf("no such global var '%s'", s)
	}

	*v = *globals[idx].loc

	return Ok
}

func (v *Value) ParseExpr(children []*Token) error {
	if len(children) == 0 {
		return fmt.Errorf("empty expression in ()")
	}

	return fmt.Errorf("cannot use expression in non-code context")
}

func (v *Value) ParseArray(children []*Token) error {
	v.MakeArray(len(children))

	for i, t := range children {
		sub := &v.Array.data[i]
		err := sub.ParseLiteral(t)
		if err != nil {
			return err
		}
	}

	return Ok
}

func (v *Value) ParseMap(children []*Token) error {
	if (len(children) % 2) != 0 {
		return fmt.Errorf("bad map literal: odd number of elements")
	}

	v.MakeMap()

	for i := 0; i < len(children); i += 2 {
		t_key := children[i]
		t_val := children[i+1]

		if !(t_key.Kind == TOK_String) {
			return fmt.Errorf("map key must be string, got: %s", t_key.String())
		}

		// NOTE : we allow duplicate keys (consistency with compiled literals)

		val := Value{}

		err := val.ParseLiteral(t_val)
		if err != nil {
			return err
		}

		v.KeySet(t_key.Str, val)
	}

	return Ok
}

//----------------------------------------------------------------------

func (v *Value) DeepString() string {
	// this keeps track of arrays/maps which have been seen, to
	// prevent infinite recursion.
	seen := make(map[*Value]bool)

	return v.rawDeepString(seen)
}

func (v *Value) rawDeepString(seen map[*Value]bool) string {
	if v.Kind == VAL_Array || v.Kind == VAL_Map {
		// already seen?
		_, ok := seen[v]
		if ok {
			return "#CyclicData#"
		}
		// mark it now
		seen[v] = true
	}

	switch v.Kind {
	case VAL_Int:
		return v.IntToString()

	case VAL_String:
		return v.StringToString()

	case VAL_Array:
		return v.ArrayToString(seen)

	case VAL_Map:
		return v.MapToString(seen)

	case VAL_Function:
		return v.FunctionToString()

	default:
		return "!!INVALID!!"
	}
}

func (v *Value) IntToString() string {
	switch v.Int {
	case NIL:
		return "NIL"
	case TRUE:
		return "TRUE"
	}

	return fmt.Sprintf("%d", v.Int)
}

func (v *Value) StringToString() string {
	if v.Str == "\x1A" {
		return "EOF"
	}

	// we need to surround the string with double quotes, and
	// escape any strange characters (including '"' itself).
	// luckily the %q verb in the fmt package does all this.

	return fmt.Sprintf("%q", v.Str)
}

func (v *Value) ArrayToString(seen map[*Value]bool) string {
	var sb strings.Builder

	sb.WriteString("[")

	for i := range v.Array.data {
		if i > 0 {
			sb.WriteString(" ")
		}

		child := v.GetElem(i)

		sb.WriteString(child.rawDeepString(seen))
	}

	sb.WriteString("]")
	return sb.String()
}

func (v *Value) MapToString(seen map[*Value]bool) string {
	var sb strings.Builder

	sb.WriteString("{")

	// collect all the names, and sort them
	keys := make([]string, 0, len(v.Map.data))

	for k := range v.Map.data {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, k int) bool {
		return keys[i] < keys[k]
	})

	for i, name := range keys {
		if i > 0 {
			sb.WriteString(" ")
		}

		// write the key
		if LEX_IsIdentifier(name) {
			sb.WriteString(".")
			sb.WriteString(name)
		} else {
			// re-use the StringToString code here
			temp := Value{Kind: VAL_String, Str: name}
			sb.WriteString(temp.StringToString())
		}

		child := v.Map.data[name]

		// write the value
		sb.WriteString(" ")

		if child == nil /* should never happen */ {
			sb.WriteString("NIL")
		} else {
			sb.WriteString(child.rawDeepString(seen))
		}
	}

	sb.WriteString("}")
	return sb.String()
}

func (v *Value) FunctionToString() string {
	return "#Function:" + v.Clos.debug_name + "#"
}
